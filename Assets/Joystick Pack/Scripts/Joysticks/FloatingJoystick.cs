﻿using UnityEngine;
using UnityEngine.EventSystems;

public class FloatingJoystick : Joystick
{
    protected override void Start()
    {
        base.Start();
        background.gameObject.SetActive(false);
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        Vector3 center = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0.5f));
        Vector3 eventScreen = Camera.main.ScreenToWorldPoint(eventData.position);
        if (eventScreen.x < center.x)
        {
            background.anchoredPosition = ScreenPointToAnchoredPosition(eventData.position);
            background.gameObject.SetActive(true);
            base.OnPointerDown(eventData); 
        }
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        bool cancel = true;
        for (int i = 0; i < Input.touchCount; i++)
        {
            if (Input.GetTouch(i).phase == TouchPhase.Stationary || Input.GetTouch(i).phase == TouchPhase.Moved)
            {
                cancel = false;
            }
        }

        if (cancel)
        {
            background.gameObject.SetActive(false);
            base.OnPointerUp(eventData);
        }
    }
}