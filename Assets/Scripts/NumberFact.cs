public static class NumberFact{

    public static string Fact(int number){
        string strNum = number.ToString();
        int numberSize = strNum.Length;
        if (numberSize>=7){
            return ((double)number/1000000).ToString("#.##") + "M";
        }
        if (numberSize>=4){
            return ((double)number/1000).ToString("#.##") + "k";
        }

        string res = ((double) number / 1).ToString("#.##");
        if (number < 1)
        {
            res = "0" + res;
        }
        return res;
    }

    public static string Fact(float fNumber)
    {
        string res = ((double) fNumber).ToString("#.##");
        if (fNumber < 1)
        {
            res = "0" + res;
        }
        return res;
    }
}