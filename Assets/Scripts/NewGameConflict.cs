using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class NewGameConflict : MonoBehaviour
{
    private Animator anim;

    public bool open;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void Trigger()
    {
        open = !open;
        if (open)
        {
            Open();
        }
        else
        {
            Close();
        }
    }

    public void Open()
    {
        anim.SetBool("Open", true);
    }

    public void Close()
    {
        anim.SetBool("Open", false);
    }

    public void Confirm()
    {
        string path = Application.persistentDataPath + "/player.ship";
        File.Delete(path);
        MainMenu.Instance.StartGame();
    }
}
