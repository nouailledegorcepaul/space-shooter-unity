using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDestruction : MonoBehaviour
{
    private Vector2 size;
    private float currentTime;
    private int amount;
    // Start is called before the first frame update
    void Start()
    {
        size = GetComponent<SpriteRenderer>().bounds.size;
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        if (currentTime > 0.1)
        {
            amount++;
            currentTime = 0;
            float randX = Random.Range(0f, size.x) - size.x/2;
            float randY = Random.Range(0f, size.y) - size.y/2;
            Vector3 pos = new Vector3(transform.position.x+randX, transform.position.y - randY, 0);
            GameObject go = Instantiate(Resources.Load("hitExplosion"), pos, Quaternion.identity) as GameObject;
        }

        // if (amount > 20)
        // {
        //     Destroy(this);
        // }
    }
}
