using UnityEngine;

public class BossBlackHoleShoot : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject shot;
    private Vector3 destination;

    void Start()
    {
        Vector3 botLeft = Camera.main.ViewportToWorldPoint(new Vector3(0, 0));
        Vector3 topRight = Camera.main.ViewportToWorldPoint(new Vector3(1, 1));

        Vector3 size = GetComponent<SpriteRenderer>().bounds.size;
        Vector3 pos = transform.position;
        Vector3 shootPos = new Vector3(pos.x - size.x/2, pos.y, pos.z);
        shot = Instantiate(Resources.Load("enemyExplosionShot"), shootPos, Quaternion.identity) as GameObject;
        shot.transform.SetParent(GameObject.FindWithTag("enemyShots").transform);
        float randX = Random.Range(botLeft.x + size.x/2, topRight.x-1.5f*size.x);
        float randY = Random.Range(botLeft.y + size.y/2, topRight.y-size.y/2);
        destination = new Vector3(randX, randY, 0);

        Destroy(shot.GetComponent<EnemyShootMovement>());
        SoundManager.Instance.ShotSound();
    }

    // Update is called once per frame
    void Update()
    {
        float speed = 2f;
        float step =  speed * Time.deltaTime; // calculate distance to move
        shot.transform.position = Vector3.MoveTowards(shot.transform.position, destination, step);

        // Check if the position of the cube and sphere are approximately equal.
        if (Vector3.Distance(shot.transform.position, destination) < 0.001f)
        {
            GameObject go = Instantiate(Resources.Load("blackHole"), destination, Quaternion.identity) as GameObject;
            go.transform.SetParent(GameObject.FindWithTag("enemyShots").transform);
            Destroy(shot);
            Destroy(this);
        };
    }
}
