using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossExplosiveShoot : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject shot;
    private float destination;
    
    void Start()
    {
        Vector3 size = GetComponent<SpriteRenderer>().bounds.size;
        Vector3 pos = transform.position;
        Vector3 shootPos = new Vector3(pos.x - size.x/2, pos.y, pos.z);
        shot =
            Instantiate(Resources.Load("enemyExplosionShot"), shootPos, Quaternion.identity) as GameObject;
        SoundManager.Instance.ShotSound();
        shot.GetComponent<EnemyShootMovement>().SetSpeed(new Vector2(-3, 0));
        Vector3 right = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        Vector3 left = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        destination = Random.Range(left.x + size.x, right.x - size.x);
    }

    // Update is called once per frame
    void Update()
    {
        if (shot.transform.position.x < destination)
        {
            Spread();
            Destroy(shot);
            Destroy(this);
        }
    }

    public void Spread()
    {
        Vector2 pos = shot.transform.position;
        GameObject go1 = Instantiate(Resources.Load("enemyShot"), pos, Quaternion.identity) as GameObject;
        GameObject go2 = Instantiate(Resources.Load("enemyShot"), pos, Quaternion.identity) as GameObject;
        GameObject go3 = Instantiate(Resources.Load("enemyShot"), pos, Quaternion.identity) as GameObject;
        GameObject go4 = Instantiate(Resources.Load("enemyShot"), pos, Quaternion.identity) as GameObject;
        GameObject go5 = Instantiate(Resources.Load("enemyShot"), pos, Quaternion.identity) as GameObject;
        GameObject go6 = Instantiate(Resources.Load("enemyShot"), pos, Quaternion.identity) as GameObject;
        GameObject go7 = Instantiate(Resources.Load("enemyShot"), pos, Quaternion.identity) as GameObject;
        GameObject go8 = Instantiate(Resources.Load("enemyShot"), pos, Quaternion.identity) as GameObject;
        go1.transform.SetParent(GameObject.FindGameObjectWithTag("enemyShots").transform);
        go2.transform.SetParent(GameObject.FindGameObjectWithTag("enemyShots").transform);
        go3.transform.SetParent(GameObject.FindGameObjectWithTag("enemyShots").transform);
        go4.transform.SetParent(GameObject.FindGameObjectWithTag("enemyShots").transform);
        go5.transform.SetParent(GameObject.FindGameObjectWithTag("enemyShots").transform);
        go6.transform.SetParent(GameObject.FindGameObjectWithTag("enemyShots").transform);
        go7.transform.SetParent(GameObject.FindGameObjectWithTag("enemyShots").transform);
        go8.transform.SetParent(GameObject.FindGameObjectWithTag("enemyShots").transform);
        
        go1.GetComponent<EnemyShootMovement>().SetSpeed(new Vector2(4,0));
        go2.GetComponent<EnemyShootMovement>().SetSpeed(new Vector2(-4,0));
        
        go3.GetComponent<EnemyShootMovement>().SetSpeed(new Vector2(0,4));
        go4.GetComponent<EnemyShootMovement>().SetSpeed(new Vector2(0,-4));
        
        go5.GetComponent<EnemyShootMovement>().SetSpeed(new Vector2(2,2));
        go6.GetComponent<EnemyShootMovement>().SetSpeed(new Vector2(-2,-2));
        go7.GetComponent<EnemyShootMovement>().SetSpeed(new Vector2(2,-2));
        go8.GetComponent<EnemyShootMovement>().SetSpeed(new Vector2(-2,2));
        
        
    }
}
