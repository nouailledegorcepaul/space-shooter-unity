using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Security.Cryptography;
using UnityEngine;

public class BossSpecialShoot : MonoBehaviour
{
    private float currentTime;

    private int currentShot, totalShot;
    // Start is called before the first frame update
    void Start()
    {
        totalShot = 10;
    }

    // Update is called once per frame
    void Update()
    {
        if (currentShot == totalShot)
        {
            Destroy(this);
        }
        currentTime += Time.deltaTime;
        if (currentTime > 0.2f)
        {
            Vector2 size = GetComponent<SpriteRenderer>().bounds.size;
            Vector3 shootPos = new Vector3(transform.position.x + size.x/2, transform.position.y, transform.position.z);
            GameObject go1 = Instantiate(Resources.Load("enemyShot"), shootPos, Quaternion.identity) as GameObject;
            GameObject go2 = Instantiate(Resources.Load("enemyShot"), shootPos, Quaternion.identity) as GameObject;
            GameObject go3 = Instantiate(Resources.Load("enemyShot"), shootPos, Quaternion.identity) as GameObject;
            GameObject go4 = Instantiate(Resources.Load("enemyShot"), shootPos, Quaternion.identity) as GameObject;
            GameObject go5 = Instantiate(Resources.Load("enemyShot"), shootPos, Quaternion.identity) as GameObject;
            
            go1.transform.SetParent(GameObject.FindGameObjectWithTag("enemyShots").transform);
            go2.transform.SetParent(GameObject.FindGameObjectWithTag("enemyShots").transform);
            go3.transform.SetParent(GameObject.FindGameObjectWithTag("enemyShots").transform);
            go4.transform.SetParent(GameObject.FindGameObjectWithTag("enemyShots").transform);
            go5.transform.SetParent(GameObject.FindGameObjectWithTag("enemyShots").transform);

            go1.GetComponent<EnemyShootMovement>().SetSpeed(new Vector2(6,3));
            go2.GetComponent<EnemyShootMovement>().SetSpeed(new Vector2(6,1.5f));
            go3.GetComponent<EnemyShootMovement>().SetSpeed(new Vector2(6,0));
            go4.GetComponent<EnemyShootMovement>().SetSpeed(new Vector2(6,-1.5f));
            go5.GetComponent<EnemyShootMovement>().SetSpeed(new Vector2(6,-3));
            currentShot++;
            currentTime = 0;
        }
    }
}
