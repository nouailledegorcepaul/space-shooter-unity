using UnityEngine;

public class BossPowerUpSpawner : MonoBehaviour
{
    private float currentTime;
    private int randTimer;
    private Vector3 topRightCamBorder, botRightCamBorder;
    // Start is called before the first frame update
    void Start()
    {
        topRightCamBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        botRightCamBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        randTimer = Random.Range(6, 9);
    }

    void Update()
    {
        if (GameManager.Instance.gameState == State.Play)
        {
            currentTime += Time.deltaTime;
            if (currentTime > randTimer)
            {
                currentTime = 0;
                randTimer = Random.Range(6, 9);
                int rand = Random.Range(1, 100);
                string prefab = "";
                GameObject ship = GameObject.FindWithTag("myShip");
                if (ship != null)
                {
                    if (ship.GetComponent<ShipStats>().GetLife() < 5)
                    {
                        prefab = "lifeBoost";
                    }
                }

                if (rand < 40)
                {
                    prefab = "multiShot";
                }
                else if (rand < 70)
                {
                    prefab = "speedBoost";
                }
                else if (rand < 90)
                {
                    prefab = "shieldBoost";
                }

                if (prefab.Length == 0)
                {
                    prefab = "multiShot";
                }

                GameObject go = Instantiate(Resources.Load(prefab), Vector3.zero, Quaternion.identity) as GameObject;
                Vector2 goSize = go.GetComponent<SpriteRenderer>().bounds.size;
                float randY = Random.Range(botRightCamBorder.y + goSize.y / 2, topRightCamBorder.y - goSize.y / 2);

                Vector3 pos = new Vector3(topRightCamBorder.x + goSize.x, randY, 0);
                go.transform.position = pos;
                go.AddComponent<BossPowerUp>();
                go.transform.SetParent(GameObject.FindGameObjectWithTag("powerUpContainer").transform);
            }
        }
    }

}