using UnityEngine;

public class BossCollider : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("shipShot"))
        {
            GameManager.Instance.RemoveHp(gameObject.GetComponent<EnemyStats>(), other.GetComponent<ShootMovement>().IsCritical());
            GameObject go = Instantiate(Resources.Load("hitExplosion"), other.gameObject.transform.position, Quaternion.identity) as GameObject;
            Destroy(other.gameObject);
        }

        if (other.CompareTag("myShip"))
        {
            GameManager.Instance.RemoveLife();
        }
    }
}
