using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHole : MonoBehaviour
{
    private float currentTime;

    private Transform ship;
    // Start is called before the first frame update
    void Start()
    {
        ship = GameObject.FindWithTag("myShip").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.gameState == State.Play)
        {
            currentTime += Time.deltaTime;
            float speed = 2f;
            float step = speed * Time.deltaTime; // calculate distance to move
            ship.position = Vector3.MoveTowards(ship.position, transform.position, step);
            float rotation = 50;
            transform.Rotate(0, 0, rotation * Time.deltaTime);
            if (currentTime > 4)
            {
                Destroy(gameObject);
            }
        }
    }
}
