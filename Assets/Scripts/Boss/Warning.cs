using UnityEngine;

public class Warning : MonoBehaviour
{
    private GameObject warning;
    private bool active;

    private float totalTime, currentTime;
    // Start is called before the first frame update
    void Start()
    {
        active = true;
        warning = GameObject.FindWithTag("warning");
        SetAlpha(1);
        SoundManager.Instance.WarningSound();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.gameState == State.Play)
        {
            currentTime += Time.deltaTime;
            totalTime += Time.deltaTime;
            if (currentTime > 0.2)
            {
                if (active)
                {
                    SetAlpha(0);
                    active = false;
                }
                else
                {
                    SetAlpha(1);
                    active = true;
                }
                currentTime = 0;
            }

            if (totalTime > 1)
            {
                SetAlpha(0);
                Destroy(this);
            } 
        }
    }

    private void SetAlpha(float transparency)
    {
        Color color = warning.GetComponent<SpriteRenderer>().color;
        color.a = transparency;
        warning.GetComponent<SpriteRenderer>().color = color;
    }
}
