using UnityEngine;

public class BossPowerUp : MonoBehaviour
{
    private Vector3 botLeftCamBorder;
    private bool shipFound;
    private float startTime;
    // Update is called once per frame

    private void Start()
    {
        botLeftCamBorder  = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        Destroy(GetComponent<PowerUp>());
        Destroy(GetComponent<SinWave>());
        Destroy(GetComponent<FadeOut>());
    }

    void Update()
    {
        if (GameManager.Instance.gameState == State.Play)
        {
            GameObject ship = GameObject.FindWithTag("myShip");
            Vector2 size = GetComponent<SpriteRenderer>().bounds.size;
            GetComponent<Rigidbody2D>().velocity = new Vector2(-2, 0);
            if (transform.position.x + size.x < botLeftCamBorder.x)
            {
                Destroy(gameObject);
            }
            if (!shipFound)
            {
                if (Vector3.Distance(transform.position, ship.transform.position) < 3)
                {
                    shipFound = true;
                    startTime = Time.time;
                }
            }
            else
            {
                Vector3 destination = ship.transform.position;
                float speed = 0.03f;
                float journeyLength = Vector3.Distance(transform.position, destination);
                float distCovered = (Time.time - startTime) * speed;
                float fractionOfJourney = distCovered / journeyLength;
                transform.position = Vector3.Slerp(transform.position, destination, fractionOfJourney);
            }  
        }
        else
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("myShip"))
        {
            switch (gameObject.tag)
            {
                case "speedBoost":
                    PowerUpManager.Instance.SetSpeedBoost();
                    break;
                case "multiShot":
                    PowerUpManager.Instance.SetMultiShot();
                    break;
                case "shieldBoost":
                    PowerUpManager.Instance.SetShieldBoost();
                    break;
                case "lifeBoost":
                    GameManager.Instance.AddPlayerLife();
                    break;
            }
            Destroy(gameObject);
        }
    }
}
