using UnityEngine;
using Random = UnityEngine.Random;

public class BossShoot : MonoBehaviour
{
    private SpriteRenderer rend;

    private float currentTime;
    private Vector3 rightBotCamBorder;
    
    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<SpriteRenderer>();
        rightBotCamBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.gameState == State.Play)
        {
            if (transform.position.x < rightBotCamBorder.x)
            {
                currentTime += Time.deltaTime;
                Vector3 size = rend.bounds.size;
                Vector3 pos = transform.position;
                Vector3 shootPos = new Vector3(pos.x - size.x/2, pos.y, pos.z);
                
                int rand = Random.Range(1, 2) - 1;
                if (currentTime > 1 + rand)
                {
                    currentTime = 0;
                    GameObject go =
                        Instantiate(Resources.Load("enemyShot"), shootPos, Quaternion.identity) as GameObject;
                    go.transform.SetParent(GameObject.FindWithTag("enemyShots").transform);
                    SoundManager.Instance.ShotSound();
                }  
            }
        }
    }
}