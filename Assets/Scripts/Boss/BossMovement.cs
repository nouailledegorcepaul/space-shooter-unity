using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossMovement : MonoBehaviour
{
    private Vector3 rightBotCamBorder, leftTopCamBorder;
    private float currentTime, randTimer;
    private bool inAction;
    private GameObject ship;
    // Start is called before the first frame update
    void Start()
    {
        ship = GameObject.FindWithTag("myShip");
        rightBotCamBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        leftTopCamBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        randTimer = Random.Range(4, 7) - 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.gameState == State.Play)
        {
            currentTime += Time.deltaTime;
            if (!inAction)
            {
                if (currentTime > 0)
                {
                    Vector2 size = GetComponent<SpriteRenderer>().bounds.size;
                    Vector3 destination = new Vector3(rightBotCamBorder.x - size.x, ship.transform.position.y, ship.transform.position.z);
                    float speed = 0.01f;
                    transform.position = Vector3.Lerp(transform.position, destination, speed);
                    PowerShot();
                    if (currentTime > 5 + randTimer)
                    {
                        inAction = true;
                        currentTime = 0;
                        Destroy(GetComponent<BossShoot>());
                        gameObject.AddComponent<Warning>();
                    } 
                }
            }
            else
            {
                Vector2 size = gameObject.GetComponent<SpriteRenderer>().bounds.size;
                if (currentTime > 4 && GetComponent<BossSpecialShoot>() == null)
                {
                    Vector2 movement = new Vector2(7, 0);
                    if (transform.position.x > rightBotCamBorder.x - size.x)
                    {
                        movement = new Vector2(0, 0);
                        inAction = false;
                        currentTime = -1;
                        gameObject.AddComponent<BossShoot>();
                    }
                    GetComponent<Rigidbody2D>().velocity = movement;
                }
                else if (currentTime > 1)
                {
                    Vector3 destination = new Vector3(leftTopCamBorder.x + size.x/2, transform.position.y, transform.position.z);
                    float speed = 12f * Time.deltaTime;
                    transform.position = Vector3.MoveTowards(transform.position, destination, speed);
                    if (transform.position.x < destination.x+0.1)
                    {
                        if (GetComponent<BossSpecialShoot>() == null)
                        {
                            gameObject.AddComponent<BossSpecialShoot>();
                        }
                    }
                }
            }
        }
    }

    public void PowerShot()
    {
        if (ObjectiveManager.Instance.currentIndex > 0)
        {
            if (Random.Range(0, 1000) == 1 && GetComponent<BossExplosiveShoot>() == null)
            {
                Debug.Log("Explosion");
                gameObject.AddComponent<BossExplosiveShoot>();
            }
        }
        if (ObjectiveManager.Instance.currentIndex > 1)
        {
            if (Random.Range(0, 1000) == 1 && GetComponent<BossBlackHoleShoot>() == null)
            {
                Debug.Log("Black Hole");
                gameObject.AddComponent<BossBlackHoleShoot>();
            }
        }
    }
}
