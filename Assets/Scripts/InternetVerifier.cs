using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class InternetVerifier : MonoBehaviour
{
    public Image internet;

    private float currentTimer;
    private bool checking;
    // Start is called before the first frame update
    void Start()
    {
        CheckInternetConnection();
    }

    // Update is called once per frame
    void Update()
    {
        if (!checking)
        {
            currentTimer += Time.deltaTime;
            if (currentTimer > 1)
            {
                CheckInternetConnection();
                currentTimer = 0;
            }
        }
    }

    public void CheckInternetConnection()
    {
        StartCoroutine(CheckInternetConnectionCo());
    }

    public IEnumerator CheckInternetConnectionCo()
    {
        checking = true;
        UnityWebRequest webrequest = UnityWebRequest.Get("https://google.com");
        webrequest.timeout = 2;
        yield return webrequest.SendWebRequest();
        if (webrequest.result == UnityWebRequest.Result.ProtocolError || webrequest.result == UnityWebRequest.Result.ConnectionError)
        {
            internet.color = new Color32(147, 34, 32, 200);

        }
        else
        {
            internet.color = new Color32(35, 183, 44, 200);
        }
        checking = false;
    }
}
