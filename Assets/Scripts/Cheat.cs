using UnityEngine;

public class Cheat : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            GameObject.FindWithTag("myShip").GetComponent<ShipStats>().AddGold(1000);
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            int goal = GameObject.FindWithTag("myShip").GetComponent<ShipStats>().GetMaxExp();
            GameObject.FindWithTag("expBar").GetComponent<ShipExperienceBar>().AddExp(goal/4);
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            int goal = ObjectiveManager.Instance.currentObjective;
            ObjectiveManager.Instance.AddAdvancement(goal/4);
        }
        if (Input.GetKeyDown(KeyCode.M) && GameManager.Instance.levelState == State.Boss)
        {
            EnemyStats s = GameObject.FindWithTag("enemy").GetComponent<EnemyStats>();
            if (s)
            {
                float goal = s.GetMaxHp();
                s.RemoveHp(goal/4);
            }
        }
    }
}
