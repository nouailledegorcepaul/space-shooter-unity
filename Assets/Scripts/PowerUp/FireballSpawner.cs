using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class FireballSpawner : MonoBehaviour
{
    public SpriteRenderer fireball;
    private Vector3 topLeftBorder, topRightBorder;
    private float currentTime;
    // Start is called before the first frame update
    void Start()
    {
        fireball = Resources.Load("fireball").GetComponent<SpriteRenderer>();
        topLeftBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        topRightBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;

        if (currentTime > 0.75)
        {
            currentTime = 0;
            Vector2 size = fireball.bounds.size;
            float randX = Random.Range(topLeftBorder.x + size.x/2, topRightBorder.x-size.x/2);
            Vector3 pos = new Vector3(randX, topLeftBorder.y + size.y, 0);
            GameObject go = Instantiate(Resources.Load("fireball"), pos, Quaternion.identity) as GameObject;
        }
    }
}
