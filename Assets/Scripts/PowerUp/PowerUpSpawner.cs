using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class PowerUpSpawner : MonoBehaviour
{
    private List<string> powerUps;
    private Vector3 topRightBorder;
    public static PowerUpSpawner Instance;
    
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        powerUps = new List<string>{"multiShot", "speedBoost", "shieldBoost", "lifeBoost"};
        topRightBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
    }

    public void SpawnPowerUp(Vector3 pos)
    {
        
        int rand = Random.Range(1, 100);
        if (rand < 25)
        {
            string prefab; ;
            if (GameObject.FindWithTag("myShip").GetComponent<ShipStats>().GetLife() < 5)
            {
                rand = Random.Range(0, powerUps.Count);
            }
            else
            {
                rand = Random.Range(0, powerUps.Count-1);
            }
            prefab = powerUps[rand];
            
            GameObject go = Instantiate(Resources.Load(prefab), pos, Quaternion.identity) as GameObject;
            Vector2 goSize = go.GetComponent<SpriteRenderer>().bounds.size;
            if (go.transform.position.x + goSize.x > topRightBorder.x)
            {
                go.transform.position = new Vector3(topRightBorder.x - goSize.x / 2,
                    go.transform.position.y, go.transform.position.z);
            }
            go.AddComponent<FadeOut>();
            go.GetComponent<FadeOut>().SetSecondsToFade(10);
                        
            go.transform.SetParent(GameObject.FindGameObjectWithTag("powerUpContainer").transform);
                        
        }
    }

    public void AddFireballPowerUp()
    {
        powerUps[powerUps.Count-1] = "fireballBoost";
        powerUps.Add("lifeBoost");
    }
}
