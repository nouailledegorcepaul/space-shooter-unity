﻿using UnityEngine;

public class Fireball : MonoBehaviour
{
    private Vector3 botLeftBorder;
    private bool isCritical;
    private float currentTime;

    // Start is called before the first frame update
    void Start()
    {
        botLeftBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        if (SkillManager.Instance.GetSkills()["fireballCriticalStrike"])
        {
            isCritical = GameObject.FindWithTag("myShip").GetComponent<ShipCrit>().IsCriticalStrike();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.gameState == State.Play)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, -2);
            if (transform.position.y + GetComponent<SpriteRenderer>().bounds.size.y < botLeftBorder.y)
            {
                Destroy(gameObject);
            }
        }
        else
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        }
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("asteroid"))
        {
            GameManager.Instance.RemoveHp(other.GetComponent<AsteroidStats>(), isCritical);
            Destroy(gameObject);
        }
    }
}