using UnityEngine;

public class PowerUp : MonoBehaviour
{
    private bool shipFound;
    private float startTime;
    
    void Update()
    {
        if (GameManager.Instance.gameState == State.Play)
        {
            GameObject ship = GameObject.FindWithTag("myShip");
            if (!shipFound)
            {
                if (Vector3.Distance(transform.position, ship.transform.position) < 3)
                {
                    shipFound = true;
                    startTime = Time.time;
                    Destroy(GetComponent<SinWave>());
                    GetComponent<FadeOut>().ResetAlpha();
                    Destroy(GetComponent<FadeOut>());
                }
            }
            else
            {
                Vector3 destination = ship.transform.position;
                float speed = 0.03f;
                float journeyLength = Vector3.Distance(transform.position, destination);
                float distCovered = (Time.time - startTime) * speed;
                float fractionOfJourney = distCovered / journeyLength;
                transform.position = Vector3.Slerp(transform.position, destination, fractionOfJourney);
            }  
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("myShip"))
        {
            switch (gameObject.tag)
            {
                case "speedBoost":
                    PowerUpManager.Instance.SetSpeedBoost();
                    break;
                case "multiShot":
                    PowerUpManager.Instance.SetMultiShot();
                    break;
                case "shieldBoost":
                    PowerUpManager.Instance.SetShieldBoost();
                    break;
                case "fireballBoost":
                    PowerUpManager.Instance.SetFireballBoost();
                    break;
                case "lifeBoost":
                    GameManager.Instance.AddPlayerLife();
                    break;
            }
            Destroy(gameObject);
        }
    }
}
