using UnityEngine;

public class SinWave : MonoBehaviour
{
    public float currentY;
    public float amplitudeY = 2.0f;
    public float omegaY = 0.5f;
    private float index;

    private void Start()
    {
        currentY = transform.position.y;
    }

    private void Update()
    {
        if (GameManager.Instance.gameState == State.Play)
        {
            index += Time.deltaTime;
            float y = Mathf.Abs(amplitudeY * Mathf.Sin(omegaY * index));
            transform.localPosition = new Vector3(transform.localPosition.x, y + currentY, 0);
        }
    }
}