using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarSpawner : MonoBehaviour
{
    private Vector3 rightTopCamBorder;
    private Vector3 rightBotCamBorder;
    
    // Start is called before the first frame update
    void Start()
    {
        rightTopCamBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        rightBotCamBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
    }

    // Update is called once per frame
    void Update()
    {
        GameObject[] currentStars = GameObject.FindGameObjectsWithTag("star");
        if (currentStars.Length < 20)
        {
            if (Random.Range(1, 50) == 25 || currentStars.Length < 10)
            {
                SpawnStar();
            }
        }
    }
    
    public void SpawnStar()
    {
        if (GameManager.Instance.gameState == State.Play)
        {
            GameObject star = Resources.Load("star") as GameObject;
            if (star != null)
            {
                Vector2 size = star.GetComponent<SpriteRenderer>().bounds.size;
                float y = Random.Range(rightBotCamBorder.y + size.y / 2, rightTopCamBorder.y - size.y / 2);
                Vector3 newPos = new Vector3(rightTopCamBorder.x + size.x, y, transform.position.z);

                GameObject go = Instantiate(star, newPos, Quaternion.identity);
                go.transform.parent = GameObject.FindGameObjectWithTag("starsContainer").transform;
            }
        }
    }
}
