using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarMovement : MonoBehaviour
{
    private SpriteRenderer rend;

    private float speed;
    private Vector3 leftBotCamBorder;
    
    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<SpriteRenderer>();
        speed = Random.Range(0.5f, 7.0f);
        leftBotCamBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 movement = new Vector2(0, 0);
        if (GameManager.Instance.gameState == State.Play)
        {
            movement = new Vector2(-speed, 0f);
        }
        GetComponent<Rigidbody2D>().velocity = movement;

        Bounds bounds = rend.bounds;
        Vector3 size = bounds.size;
        float sizeX = size.x;
        Vector3 pos = transform.position;
        if (pos.x + (sizeX / 2) < leftBotCamBorder.x)
        {
            Destroy(gameObject);
        }
    }
}
