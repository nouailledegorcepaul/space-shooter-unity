using UnityEngine;
using UnityEngine.UI;

public class UsernameCreator : MonoBehaviour
{
    public static UsernameCreator Instance;
    public InputField inputField;
    public Animator anim;
    public string target;
    public bool creatorOpen;
    
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        inputField.characterLimit = 20;
        creatorOpen = false;
    }

    public void Open(string t)
    {
        anim.SetBool("Open", true);
        target = t;
        creatorOpen = true;
    }

    public void Close()
    {
        anim.SetBool("Open", false);
        creatorOpen = false;
    }
    
    public void SubmitUsernameToPlayFab()
    {
        if (inputField.text.Length >= 3)
        {
            PlayFabManager.Instance.GetCurrentPlayer();
            PlayFabManager.Instance.SetPlayFabUserName(inputField.text);
            Close();
            switch (target)
            {
                case "start":
                    MainMenu.Instance.StartGame();
                    break;
                case "load":
                    MainMenu.Instance.LoadGame();
                    break;
                case "leaderboard":
                    PlayFabManager.Instance.Trigger();
                    break;
            }
        }
    }
}
