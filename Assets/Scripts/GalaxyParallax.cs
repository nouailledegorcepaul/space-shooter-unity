using UnityEngine;

public class GalaxyParallax : MonoBehaviour
{

    public bool leftToRight;
    public float xRestartPos;
    public Vector2 movement;
    private Vector3 leftBottomBorder;
    private Vector3 rightBottomBorder;

    private void Start()
    {
        leftBottomBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        rightBottomBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
    }

    void Update()
    {
        if (GameManager.Instance.gameState == State.Play)
        {
            GetComponent<Rigidbody2D>().velocity = movement;
            Vector2 size = GetComponent<SpriteRenderer>().bounds.size;
            if (leftToRight)
            {
                if	(transform.position.x - size.x/2 > rightBottomBorder.x)
                {	
                    transform.position = new Vector3(xRestartPos, transform.position.y,transform.position.z);	
                }   
            }
            else
            {
                if	(transform.position.x + size.x/2 < leftBottomBorder.x)
                {	
                    transform.position = new Vector3(xRestartPos, transform.position.y,transform.position.z);	
                }  
            }
        }
        else
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }
    }	
}