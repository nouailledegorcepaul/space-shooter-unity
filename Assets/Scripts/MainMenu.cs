using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public static MainMenu Instance;
    public GameObject startGo, continueGo;
    public NewGameConflict newGame;
    public TutorialScreen tutorial;
    public PlayFabManager leaderboard;
    private float currentTimer;
    public Sprite yellow, red, green;
    
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }
    
    void Start()
    {
        string path = Application.persistentDataPath + "/player.ship";
        if (File.Exists(path))
        {
            startGo.GetComponent<Image>().sprite = yellow;
            continueGo.GetComponent<Image>().sprite = green;
            continueGo.GetComponent<Button>().interactable = true;
        }
        else
        {
            continueGo.GetComponent<Image>().sprite = red;
            continueGo.GetComponent<Button>().interactable = false;
        }
    }

    public void StartGame()
    {
        if (PlayFabManager.Instance.username.Length > 0)
        {
            string path = Application.persistentDataPath + "/player.ship";
            if (File.Exists(path)){
                Debug.Log("File already exists. TODO Confirm window");
                newGame.Trigger();
            }
            else
            {
                GoToGame();  
            }
        }
        else
        {
            UsernameCreator.Instance.Open("start");
        }
    }

    public void LoadGame()
    {
        if (PlayFabManager.Instance.username.Length > 0)
        {
            GameObject.FindWithTag("saveLoader").AddComponent<SaveLoader>();
            GoToGame();
        }
        else
        {
            UsernameCreator.Instance.Open("load");
        }
    }

    public void GoToGame()
    {
        SceneManager.LoadScene("GameScene");
        SoundManager.Instance.PlayGameMusic();
    }
}
