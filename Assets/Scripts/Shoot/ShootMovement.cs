using UnityEngine;

public class ShootMovement : MonoBehaviour
{
    private SpriteRenderer rend;
    private bool isCritical;
    private Vector3 rightBotCamBorder;
    public Vector2 speed;
    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<SpriteRenderer>();
        rightBotCamBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        isCritical = GameObject.FindWithTag("myShip").GetComponent<ShipCrit>().IsCriticalStrike();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 movement = new Vector2(0, 0);
        if (GameManager.Instance.gameState == State.Play)
        {
            movement = speed;
        }
        GetComponent<Rigidbody2D>().velocity = movement;

        Bounds bounds = rend.bounds;
        Vector3 size = bounds.size;
        float sizeX = size.x;
        Vector3 pos = transform.position;
        if (pos.x - (sizeX / 2) > rightBotCamBorder.x)
        {
            Destroy(gameObject);
        }
    }

    public bool IsCritical()
    {
        return isCritical;
    }
    
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider!=null)
        {
            if (collider.gameObject.CompareTag("asteroid"))
            {
                if (collider.GetComponent<AsteroidStats>().IsAlive())
                {
                    Vector3 pos = transform.position;
                    GameObject go = Instantiate(Resources.Load("hitExplosion"), pos, Quaternion.identity) as GameObject;
                    GameManager.Instance.RemoveHp(collider.GetComponent<AsteroidStats>(), isCritical);
                    Destroy(gameObject);
                }
            }
        }
    }
}
