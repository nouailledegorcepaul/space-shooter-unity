using UnityEngine;

public class MultiShoot : MonoBehaviour
{
    private SpriteRenderer rend;
    private float currentTime;
    
    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.gameState == State.Play)
        {
            Bounds bounds = rend.bounds;
            Vector3 size = bounds.size;
            float sizeX = size.x;
            float sizeY = size.y;
            Vector3 pos = transform.position;
            if (GetComponent<ShipStats>().CanAttack())
            {
                bool canShoot = false;
                if (ControlManager.Instance.controlState == ControlState.TouchScreen)
                {
                    Vector3 center = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0.5f));
                    for (int i = 0; i < Input.touchCount; i++)
                    {
                        Touch touch = Input.GetTouch(i);
                        if (touch.phase == TouchPhase.Ended)
                        {
                            Vector3 eventScreen = Camera.main.ScreenToWorldPoint(Input.GetTouch(i).position);
                            if (eventScreen.x >= center.x)
                            {
                                canShoot = true;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    canShoot = Input.GetKey(KeyCode.Space);
                }
                if (canShoot)
                {
                    Vector3 shootPos1 = new Vector3(pos.x + sizeX/2, pos.y + (sizeY/4), pos.z);
                    Vector3 shootPos2 = new Vector3(pos.x + sizeX/2, pos.y - (sizeY/4), pos.z);
                    GameObject go1 =
                        Instantiate(Resources.Load("lightBlueShot"), shootPos1, Quaternion.identity) as GameObject;
                    GameObject go2 =
                        Instantiate(Resources.Load("lightBlueShot"), shootPos2, Quaternion.identity) as GameObject;
                    SoundManager.Instance.ShotSound();
                    GetComponent<ShipStats>().SetCanAttack(false);
                }
            }
        }
    }
}
