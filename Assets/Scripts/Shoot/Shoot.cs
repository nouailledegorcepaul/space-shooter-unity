using UnityEngine;

public class Shoot : MonoBehaviour
{
    private SpriteRenderer rend;
    private float currentTime;
    
    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.gameState == State.Play)
        {
            Bounds bounds = rend.bounds;
            Vector3 size = bounds.size;
            float sizeX = size.x;
            Vector3 pos = transform.position;
            if (GetComponent<ShipStats>().CanAttack())
            {
                bool canShoot = false;
                if (ControlManager.Instance.controlState == ControlState.TouchScreen)
                {
                    Vector3 center = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0.5f));
                    print(Input.touchCount);
                    for (int i = 0; i < Input.touchCount; i++)
                    {
                        Touch touch = Input.GetTouch(i);
                        print(touch.phase);
                        Vector3 eventScreen = Camera.main.ScreenToWorldPoint(Input.GetTouch(i).position);
                        if (eventScreen.x >= center.x){
                            if (touch.phase == TouchPhase.Ended)
                            {
                                print("Shoot");
                                canShoot = true;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    canShoot = Input.GetKey(KeyCode.Space);
                }
                if (canShoot)
                {
                    Vector3 shootPos = new Vector3(pos.x + sizeX/2, pos.y, pos.z);
                    GameObject go =
                        Instantiate(Resources.Load("blueShot"), shootPos, Quaternion.identity) as GameObject;
                    SoundManager.Instance.ShotSound();
                    GetComponent<ShipStats>().SetCanAttack(false);
                }
            }
        }
    }
}
