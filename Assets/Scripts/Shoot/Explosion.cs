using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Animator anim = GetComponent<Animator>();
        Invoke(nameof(DestroyExplosion), anim.GetCurrentAnimatorStateInfo(0).length);
    }

    void DestroyExplosion()
    {
        Destroy(gameObject);
    }
}
