using UnityEngine;

public class ScoreMovement : MonoBehaviour
{
    private Vector3 destination;
    
    private float speed = 0.01f;
    private float startTime;
    

    void Start()
    {
        Vector3 rightTopCamBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        Vector3 leftTopCamBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        Vector3 size = GetComponent<SpriteRenderer>().bounds.size;
        startTime = Time.time;
        // Code below replaces gameObject so it won't be out of the viewport
        float xMax = rightTopCamBorder.x - size.x / 2;
        float xMin = leftTopCamBorder.x + size.x / 2;
        float destX = transform.position.x;
        if (destX > xMax)
        {
            transform.position = new Vector3(xMax, transform.position.y, transform.position.z);
        }
        else if (destX < xMin)
        {
            transform.position = new Vector3(xMin, transform.position.y, transform.position.z);
        }
        
        float yDistance = size.y;
        float yMax = rightTopCamBorder.y - size.y / 2;
        float destY = transform.position.y + yDistance;
        if (destY > yMax)
        {
            destY = yMax;
        }
        destination = new Vector3(transform.position.x, destY, transform.position.z);
    }

    // Move to the target end position.
    void Update()
    {
        float journeyLength = Vector3.Distance(transform.position, destination);

        float distCovered = (Time.time - startTime) * speed;
        float fractionOfJourney = distCovered / journeyLength;
        transform.position = Vector3.Lerp(transform.position, destination, fractionOfJourney);

        if (transform.position == destination)
        {
            Destroy(this.gameObject);
        }
    }
}