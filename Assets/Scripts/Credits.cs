using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Credits : MonoBehaviour
{
    public GameObject creditsGo;
    private bool open;

    void Start()
    {
        open = false;
    }
    
    public void Trigger()
    {
        open = !open;
        if (open)
        {
            Open();
        }
        else
        {
            Close();
        }
    }

    private void Open()
    {
        creditsGo.SetActive(true);
    }

    private void Close()
    {
        creditsGo.SetActive(false);
    }
}

