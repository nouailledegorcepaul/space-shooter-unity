using UnityEngine;

public class FadeOut : MonoBehaviour
{
    public float secondsToFade = 0.5f;
    public float currentTime;
    private Color spriteColor;
    
    void Start()
    {
        spriteColor = GetComponent<SpriteRenderer>().color;
    }
    
    void Update()
    {
        if (gameObject.CompareTag("myShip"))
        {
            currentTime += Time.deltaTime;
            GameManager.Instance.gameState = State.Pause;
            secondsToFade = 2f;
            spriteColor.a -= 1 - currentTime / secondsToFade;
            GetComponent<SpriteRenderer>().color = spriteColor;
            if (spriteColor.a < 0)
            {
                GameManager.Instance.RespawnShip();
                GameManager.Instance.gameState = State.Play;
                Destroy(this);
            }
        }
        else if (gameObject.CompareTag("enemy"))
        {
            currentTime += Time.deltaTime;
            GameManager.Instance.gameState = State.Pause;
            secondsToFade = 5f;
            spriteColor.a = 1 - currentTime / secondsToFade;
            GetComponent<SpriteRenderer>().color = spriteColor;
            if (spriteColor.a < 0)
            {
                ObjectiveManager.Instance.NextObjective();
                GameManager.Instance.SpawnEnemy();
                GameManager.Instance.SetAsteroidSpawnWork(true);
                GameManager.Instance.levelState = State.Level;
                GameManager.Instance.gameState = State.Play;
                SoundManager.Instance.PlayGameMusic();
                Destroy(gameObject);
            }
        }
        else
        {
            if (GameManager.Instance.gameState == State.Play)
            {
                currentTime += Time.deltaTime;
                spriteColor.a = 1 - currentTime / secondsToFade;
                spriteColor.a -= 0.01f / secondsToFade;
                GetComponent<SpriteRenderer>().color = spriteColor;
                if (spriteColor.a < 0)
                {
                    Destroy(gameObject);
                }
            } 
        }
    }

    public void SetSecondsToFade(float seconds)
    {
        secondsToFade = seconds;
    }

    public void ResetAlpha()
    {
        Color color = GetComponent<SpriteRenderer>().color;
        color.a = 1f;
        GetComponent<SpriteRenderer>().color = color;
    }
}
