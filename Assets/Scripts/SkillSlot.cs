using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillSlot : MonoBehaviour
{
    public GameObject buyButton;
    public string skillName;
    public string skill;
    [TextArea(1,5)]
    public string description;
    public SkillSlot[] requirements;
    public int price;
    private ShipStats stats;
    public Sprite greenButton, redButton;
    private bool purchased;
    private bool available;
    
    // Start is called before the first frame update
    void Start()
    {
        stats = GameObject.FindWithTag("myShip").GetComponent<ShipStats>();
        buyButton.GetComponentInChildren<Text>().text = ""+NumberFact.Fact(price);
    }

    // Update is called once per frame
    void Update()
    {
        purchased = SkillManager.Instance.GetSkills()[skill];
        buyButton.GetComponent<Image>().sprite = stats.GetGold() >= price ? greenButton : redButton;
        if (purchased)
        {
            UnlockSkill();
            GetComponentInChildren<Text>().text = description;
            buyButton.GetComponentInChildren<Text>().text = "Purchased";
            buyButton.GetComponent<Image>().sprite = greenButton;
            buyButton.GetComponent<Button>().interactable = false;
            transform.SetAsLastSibling();
            Destroy(this);
        }
        else if (!available)
        {
            bool testAvailable = true;
            string reqToDisplay = "Requirements: ";
            foreach (SkillSlot req in requirements)
            {
                if (!SkillManager.Instance.GetSkills()[req.skill])
                {
                    testAvailable = false;
                    reqToDisplay += req.skillName + ", ";
                }
            }

            if (testAvailable)
            {
                available = true;
                GetComponentInChildren<Text>().text = description;
                buyButton.GetComponent<Button>().interactable = true;
            }
            else
            {
                reqToDisplay = reqToDisplay.Substring(0, reqToDisplay.Length -2);
                GetComponentInChildren<Text>().text = reqToDisplay;
                buyButton.GetComponent<Button>().interactable = false;
            }
        }
    }

    public void Buy()
    {
        if (stats.GetGold() >= price)
        {
            gameObject.transform.SetAsLastSibling();
            buyButton.GetComponent<Button>().interactable = false;
            stats.SpendGold(price);
            UnlockSkill();
            
        }
    }

    public void UnlockSkill()
    {
        switch (skill)
        {
            case "fireball":
                PowerUpSpawner.Instance.AddFireballPowerUp();
                break;
            case "criticalStrike":
                stats.gameObject.GetComponent<ShipCrit>().AddCritChance(10);
                stats.gameObject.GetComponent<ShipCrit>().AddCritDamage(0.75f);
                break;
        }
        SkillManager.Instance.UnlockSkill(skill);
    }
}
