using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvincibleShip : MonoBehaviour
{
    
    private float current, total, max;
    private SpriteRenderer sprite;
    
    // Start is called before the first frame update
    void Start()
    {
        sprite = GameObject.FindWithTag("myShip").GetComponent<SpriteRenderer>();
        max = 1.5f;
    }

    // Update is called once per frame
    void Update()
    {
        current += Time.deltaTime;
        total += Time.deltaTime;
        if (current < 0.25f)
        {
            sprite.enabled = false;  
        }
        else
        {
            sprite.enabled = true;
        }

        if (current > 0.5f)
        {
            current = 0;
        }
        if (total >= max)
        {
            Destroy(this);
        }
    }
}
