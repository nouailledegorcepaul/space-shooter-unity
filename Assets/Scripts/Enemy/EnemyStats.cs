using System;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class EnemyStats : MonoBehaviour
{
    public float hp, maxHp;
    public int goldOnKill;
    public int expOnKill;
    private Vector3 healthBarLocalScale, rightBotCamBorder, rightTopCamBorder;
    public GameObject healthBar;
    private float dist;
    public double timer, currentTimer;

    private void Start(){
        maxHp = hp;
        Vector2 size = GetComponent<SpriteRenderer>().bounds.size;
        GameObject healthBar = Resources.Load("healthBar") as GameObject;
        
        float y = Random.Range(rightBotCamBorder.y + size.y / 2, rightTopCamBorder.y - size.y / 2);
        Vector3 newPos = new Vector3(rightTopCamBorder.x + 2*size.x, y, transform.position.z);
        GameObject hb = Instantiate(healthBar, newPos, Quaternion.identity);
        SetHealthBar(hb);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.gameState == State.Play)
        {
            if (GameManager.Instance.levelState == State.Boss)
            {
                if (IsAlive())
                {
                    currentTimer += Time.deltaTime;
                    int timerToDisplay = (int)timer - (int)Math.Round(currentTimer);
                    if (timerToDisplay < 0)
                    {
                        timerToDisplay = 0;
                    }
                    ObjectiveManager.Instance.bossTimer.GetComponent<Text>().text = "Timer: " + timerToDisplay;
                    if (currentTimer > timer)
                    {
                        GameManager.Instance.RespawnShipTimeUp();
                    }
                }
            }
            if (healthBar != null)
            {
                if (hp < 0)
                {
                    hp = 0;
                }

                float fill = hp / maxHp;
                if (fill < 1)
                {
                    healthBar.SetActive(true);
                    healthBarLocalScale.x = fill;
                    healthBar.GetComponent<SpriteRenderer>().transform.localScale = healthBarLocalScale;
                }
                else
                {
                    healthBar.SetActive(false);
                }
                Vector3 goPos = gameObject.transform.position;
                healthBar.transform.position = new Vector3(goPos.x, goPos.y + dist, goPos.z);
                if (!IsAlive())
                {
                    if (GetComponent<FadeOut>() == null)
                    {
                        gameObject.AddComponent<FadeOut>();
                        GameManager.Instance.AddPlayerExp(expOnKill);
                        GameManager.Instance.AddPlayerGold(goldOnKill);
                        DestroyHealthBar();
                    }
                }   
            }
        }
    }

    public float GetMaxHp()
    {
        return maxHp;
    }
    
    public void SetMaxHp(int max)
    {
        maxHp = max;
        hp = max;
    }

    public void RemoveHp(float amount)
    {
        hp -= amount;
    }
    public bool IsAlive()
    {
        return hp > 0;
    }

    public void SetHealthBar(GameObject go)
    {
        Vector3 topRightCamBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        healthBar = go;
        healthBarLocalScale = healthBar.GetComponent<SpriteRenderer>().transform.localScale;
        Vector2 size = healthBar.GetComponent<SpriteRenderer>().bounds.size;
        if (healthBar.GetComponent<SpriteRenderer>().transform.position.y + size.y/2 > topRightCamBorder.y)
        {
            Vector3 currPos = healthBar.GetComponent<SpriteRenderer>().transform.localScale;
            currPos.y = -currPos.y;
            healthBar.GetComponent<SpriteRenderer>().transform.localScale = currPos;
        }
        Vector2 size2 = GetComponent<SpriteRenderer>().bounds.size;
        float d = size2.y / 2 + 0.1f;
        dist = d;
    }

    public void DestroyHealthBar()
    {
        Destroy(healthBar);
    }
}
