using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    private GameObject ship;
    private Vector3 rightBotCamBorder;
    // Start is called before the first frame update
    void Start()
    {
        ship = GameObject.FindWithTag("myShip");
        rightBotCamBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.gameState == State.Play)
        {
            Vector2 size = GetComponent<SpriteRenderer>().bounds.size;
            Vector3 destination = new Vector3(rightBotCamBorder.x - size.x, ship.transform.position.y, ship.transform.position.z);
            float speed = 0.01f;
            transform.position = Vector3.Slerp(transform.position, destination, speed);
        }
    }
}
