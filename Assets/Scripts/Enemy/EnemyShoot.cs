using UnityEngine;
using Random = UnityEngine.Random;

public class EnemyShoot : MonoBehaviour
{
    private SpriteRenderer rend;

    private float currentTime;
    private Vector3 rightBotCamBorder;
    // private bool inBossLevel;
    
    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<SpriteRenderer>();
        rightBotCamBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        // inBossLevel = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.gameState == State.Play)
        {
            if (transform.position.x < rightBotCamBorder.x)
            {
                currentTime += Time.deltaTime;
                Vector3 size = rend.bounds.size;
                Vector3 pos = transform.position;
                Vector3 shootPos = new Vector3(pos.x - size.x/2, pos.y, pos.z);
                
                int rand = Random.Range(5, 7) - 1;
                if (currentTime > 3 + rand)
                {
                    currentTime = 0;
                    GameObject go =
                        Instantiate(Resources.Load("enemyShot"), shootPos, Quaternion.identity) as GameObject;
                    SoundManager.Instance.ShotSound();
                }  
            }
        }
    }
}
