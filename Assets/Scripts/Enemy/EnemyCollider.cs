﻿using UnityEngine;

public class EnemyCollider : MonoBehaviour
{
   private void OnTriggerEnter2D(Collider2D other)
   {
      if (other.CompareTag("myShip"))
      {
         GameManager.Instance.RemoveLife();
      }
      else if (other.CompareTag("shipShot") || other.CompareTag("fireball"))
      {
         GameObject go = Instantiate(Resources.Load("hitExplosion"), other.transform.position, Quaternion.identity) as GameObject;
         if (SkillManager.Instance.GetSkills()["enemyGoldOnHit"])
         {
            int goldOnHit = ObjectiveManager.Instance.GetAsteroids()[0].GetComponent<AsteroidStats>().goldOnKill/3;
            if (goldOnHit == 0)
            {
               goldOnHit = 1;
            }
            GameManager.Instance.AddPlayerGold(goldOnHit);
         }
         Destroy(other.gameObject);
      }
   }
}