using UnityEngine;

public class EnemyShootMovement : MonoBehaviour
{
    private SpriteRenderer rend;
    
    private Vector3 leftBotCamBorder, rightTopCamBorder;
    public Vector2 speed;
    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<SpriteRenderer>();
        leftBotCamBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        rightTopCamBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 movement = new Vector2(0, 0);
        if (GameManager.Instance.gameState == State.Play)
        {
            movement = speed;
        }
        GetComponent<Rigidbody2D>().velocity = movement;

        Bounds bounds = rend.bounds;
        Vector3 size = bounds.size;
        float sizeX = size.x;
        float sizeY = size.x;
        Vector3 pos = transform.position;
        if (pos.x + (sizeX / 2) < leftBotCamBorder.x || pos.x - (sizeX / 2) > rightTopCamBorder.x)
        {
            Destroy(gameObject);
        }
        else if (pos.y - (sizeY / 2) < leftBotCamBorder.y || pos.y + (sizeY / 2) > rightTopCamBorder.y)
        {
            Destroy(gameObject);
        }
    }
    
    public void SetSpeed(Vector2 spd)
    {
        speed = spd;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other!=null)
        {
            if (other.gameObject.CompareTag("myShip"))
            {
                Vector3 pos = transform.position;
                GameObject go = Instantiate(Resources.Load("hitExplosion"), pos, Quaternion.identity) as GameObject;
                GameManager.Instance.RemoveLife();
                Destroy(gameObject);
            }
        }
    }
}