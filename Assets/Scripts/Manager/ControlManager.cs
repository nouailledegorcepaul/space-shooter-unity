using UnityEngine;
using UnityEngine.UI;

public enum ControlState
{
    Keyboard,
    TouchScreen
}
public class ControlManager : MonoBehaviour
{

    public static ControlManager Instance;
    public ControlState controlState;
    public Image keyboardSprite, touchScreenSprite;
    
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
    }

    private void Start()
    {
        SetKeyboardControl();
    }

    public void SetKeyboardControl()
    {
        controlState = ControlState.Keyboard;
        SetAlpha(keyboardSprite, 1);
        SetAlpha(touchScreenSprite, 0.5f);
        
    }

    public void SetTouchScreenControl()
    {
        controlState = ControlState.TouchScreen;
        SetAlpha(touchScreenSprite, 1);
        SetAlpha(keyboardSprite, 0.5f);
    }

    public void SetAlpha(Image image, float alpha)
    {
        Color color = image.color;
        color.a = alpha;
        image.color = color;
    }
}
