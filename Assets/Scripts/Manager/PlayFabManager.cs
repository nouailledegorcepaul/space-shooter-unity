using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine.UI;

public class PlayFabManager : MonoBehaviour
{

    public static PlayFabManager Instance;
    public Transform rowsParent;
    public Animator anim;
    public bool leaderBoardOpen;
    public string username;
    private float currentTime;
    private PlayerLeaderboardEntry user;
    
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            print("Passe");
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
        DontDestroyOnLoad(this);
    }

    // Start is called before the first frame update
    void Start()
    {
        Login();
        PlayerData data = SaveSystem.LoadPlayer();
        if (data != null)
        {
            username = data.username;
        }
        
    }

    public void Login()
    {
        LoginWithCustomIDRequest request = new LoginWithCustomIDRequest
        {
            CustomId = SystemInfo.deviceUniqueIdentifier,
            CreateAccount = true
        };
        PlayFabClientAPI.LoginWithCustomID(request, OnSuccess, OnError);
    }

    private void OnSuccess(LoginResult result)
    {
        Debug.Log("Successful login/account create");
        GetCurrentPlayer();
    }

    private void OnError(PlayFabError error)
    {
        Debug.Log("Error while logging in/ creating account");
        Debug.Log(error.GenerateErrorReport());
    }

    public void SetPlayFabUserName(string name)
    {
        username = name;
        var request = new UpdateUserTitleDisplayNameRequest()
        {
            DisplayName = username
        };
        PlayFabClientAPI.UpdateUserTitleDisplayName(request, OnUserNameUpdate, OnError);
    }

    public void OnUserNameUpdate(UpdateUserTitleDisplayNameResult result)
    {
        SendLeaderboard(0);
        Debug.Log("Name successfully set: " + result.DisplayName);
    }
    
    public void SendLeaderboard(int score)
    {
        var request = new UpdatePlayerStatisticsRequest()
        {
            Statistics = new List<StatisticUpdate>
            {
                new StatisticUpdate
                {
                    StatisticName = "ShipLevel",
                    Value = score
                }
            }
        };
        PlayFabClientAPI.UpdatePlayerStatistics(request, OnLeaderboardUpdate, OnError);
    }

    private void OnLeaderboardUpdate(UpdatePlayerStatisticsResult result)
    {
        Debug.Log("Successful leaderboard sent");
    }

    public void GetLeaderboard()
    {
        if (!PlayFabClientAPI.IsClientLoggedIn())
        {
            Login();
        }
        var request = new GetLeaderboardRequest()
        {
            StatisticName = "ShipLevel",
            StartPosition = 0,
            MaxResultsCount = 7
        };
        PlayFabClientAPI.GetLeaderboard(request, OnLeaderboardGet, OnError);
    }

    public void GetCurrentPlayer()
    {
        var request = new GetLeaderboardAroundPlayerRequest()
        {
            StatisticName = "ShipLevel",
            MaxResultsCount = 1
        };
        PlayFabClientAPI.GetLeaderboardAroundPlayer(request, OnPlayerIndexGet, OnError);
    }

    private void OnPlayerIndexGet(GetLeaderboardAroundPlayerResult result)
    {
        user = result.Leaderboard[0];
    }

    private void OnLeaderboardGet(GetLeaderboardResult result)
    {
        foreach (Transform t in rowsParent)
        {
            Destroy(t.gameObject);
        }
        bool foundPlayer = false;
        for (int i = 0; i < result.Leaderboard.Count; i++)
        {
            PlayerLeaderboardEntry item = result.Leaderboard[i];
            if (item.PlayFabId == user.PlayFabId)
            {
                foundPlayer = true;
            }
            if (i == 6 && !foundPlayer)//7 values maximum, the last one is the sixth iteration
            {
                item = user;
            }
            GameObject go = Instantiate(Resources.Load("Row"), rowsParent) as GameObject;
            Text[] texts = go.GetComponentsInChildren<Text>();
            texts[0].text = (item.Position + 1).ToString();
            texts[1].text = item.PlayFabId;
            if (item.DisplayName != null)
            {
                texts[1].text = item.DisplayName;
            }
            texts[2].text = item.StatValue.ToString();
        }
    }

    public void Trigger()
    {
        leaderBoardOpen = !leaderBoardOpen;
        if (leaderBoardOpen)
        {
            if (MainMenu.Instance.newGame.open)
            {
                MainMenu.Instance.newGame.Trigger();
            }
            if (SoundManager.Instance.volumeOpen)
            {
                SoundManager.Instance.TriggerVolume();
            }
            Open();
        }
        else
        {
            Close();
        }
    }
    public void Open()
    {
        if (username.Length > 0)
        {
            GetLeaderboard();
            anim.SetBool("Open", true);
        }
        else
        {
            UsernameCreator.Instance.Open("leaderboard");
        }
    }

    public void Close()
    {
        anim.SetBool("Open", false);
    }
}
