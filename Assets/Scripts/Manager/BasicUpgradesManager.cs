using UnityEngine;
using UnityEngine.UI;

public class BasicUpgradesManager : MonoBehaviour
{
    private ShipStats stats;
    private BasicUpgradesManager Instance;
    private bool open;
    private float attackAmount, attackSpeedAmount, speedAmount;
    private int attackCost, attackSpeedCost, speedCost;
    public GameObject basicUpgradesPanel;
    public int attackLvl, attackSpeedLvl, speedLvl;
    public Sprite greenButton, redButton;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            print("Passe");

            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            attackAmount = 0.1f;
            attackSpeedAmount = 0.03f;
            speedAmount = 0.05f;
            attackCost = 10;
            attackSpeedCost = 10;
            speedCost = 30;
            attackLvl = 1;
            attackSpeedLvl = 1;
            speedLvl = 1;
        }
    }

    private void Start()
    {
        stats = GameObject.FindWithTag("myShip").GetComponent<ShipStats>();
    }

    private void Update()
    {
        if (basicUpgradesPanel.activeSelf)
        {
            Image attackButtonImage = GameObject.FindWithTag("attackButton").GetComponent<Image>();
            Image attackSpeedButtonImage = GameObject.FindWithTag("attackSpeedButton").GetComponent<Image>();
            Image speedButtonImage = GameObject.FindWithTag("speedButton").GetComponent<Image>();

            attackButtonImage.sprite = stats.GetGold() >= attackCost ? greenButton : redButton;
            attackSpeedButtonImage.sprite = stats.GetGold() >= attackSpeedCost ? greenButton : redButton;
            speedButtonImage.sprite = stats.GetGold() >= speedCost ? greenButton : redButton;
            
            GameObject.FindWithTag("attackButton").GetComponentInChildren<Text>().text = "" + NumberFact.Fact(attackCost);
            GameObject.FindWithTag("attackSpeedButton").GetComponentInChildren<Text>().text = "" + NumberFact.Fact(attackSpeedCost);
            GameObject.FindWithTag("speedButton").GetComponentInChildren<Text>().text = "" + NumberFact.Fact(speedCost);

            float attackToDisplay = Mathf.Round(stats.GetAttack() * 100.0f) * 0.01f;
            float attackSpeedToDisplay = Mathf.Round(stats.GetAttackSpeed() * 100.0f) * 0.01f;
            float speedToDisplay = Mathf.Round(stats.GetSpeed() * 100.0f) * 0.01f;
            
            GameObject.FindWithTag("attackText").GetComponent<Text>().text = "Atk: " + NumberFact.Fact(attackToDisplay);
            if (PowerUpManager.Instance.HasMultiShot())
            {
                attackToDisplay = Mathf.Round(stats.GetAttack()* 0.75f * 100.0f) * 0.01f;
                GameObject.FindWithTag("attackText").GetComponent<Text>().text = "Atk: " + NumberFact.Fact(attackToDisplay) + " (x2)"; 
            }
            GameObject.FindWithTag("attackSpeedText").GetComponent<Text>().text = "Atk Spd: " + NumberFact.Fact(attackSpeedToDisplay);
            GameObject.FindWithTag("speedText").GetComponent<Text>().text = "Speed: " + NumberFact.Fact(speedToDisplay);
        }
    }

    public int GetAttackLevel() { return attackLvl; }
    public int GetAttackSpeedLevel() { return attackSpeedLvl; }
    public int GetSpeedLevel() { return speedLvl; }
    

    public void BuyAttack()
    {
        if (stats.GetGold() >= attackCost)
        {
            stats.SpendGold(attackCost);
            stats.AddBonusAttack(attackAmount);
            CalculateNextAttackCost();
            stats.RefreshStats();
        }
    }
    
    public void BuyAttackSpeed()
    {
        if (stats.GetGold() >= attackSpeedCost)
        {
            stats.SpendGold(attackSpeedCost);
            stats.AddBonusAttackSpeed(attackSpeedAmount);
            CalculateNextAttackSpeedCost();
            stats.RefreshStats();
        }
    }
    
    public void BuySpeed()
    {
        if (stats.GetGold() >= speedCost)
        {
            stats.SpendGold(speedCost);
            stats.AddBonusSpeed(speedAmount);
            CalculateNextSpeedCost();
            stats.RefreshStats();
        }
    }

    public void CalculateNextAttackCost()
    {
        attackLvl++;
        float mult = 1.4f;
        if (attackLvl % 10 == 0)
        {
            mult = 2.5f;
            attackAmount *= 5;
        }
        attackCost = (int)(attackCost * mult);
        
    }
    
    public void CalculateNextAttackSpeedCost()
    {
        attackSpeedLvl++;
        float mult = 1.4f;
        if (attackSpeedLvl % 10 == 0)
        {
            mult = 2.5f;
            attackSpeedAmount *= 2;
        }
        attackSpeedCost = (int)(attackSpeedCost * mult);
    }
    
    public void CalculateNextSpeedCost()
    {
        speedLvl++;
        float mult = 3f;
        if (speedLvl % 10 == 0)
        {
            mult = 5f;
            speedAmount *= 1.5f;
        }
        speedCost = (int)(speedCost * mult);
    }
    
    public void CalculateAllCost(int aLvl, int asLvl, int sLvl)
    {
        attackCost = 10;
        attackSpeedCost = 10;
        speedCost = 30;

        for (int i = 1; i < aLvl; i++)
        {
            stats.AddBonusAttack(attackAmount);
            CalculateNextAttackCost();

        }
        for (int i = 1; i < asLvl; i++)
        {
            stats.AddBonusAttackSpeed(attackSpeedAmount);
            CalculateNextAttackSpeedCost();
        }
        for (int i = 1; i < sLvl; i++)
        {
            stats.AddBonusSpeed(speedAmount);
            CalculateNextSpeedCost();
        }
    }
}
