﻿using System.Collections.Generic;
using UnityEngine;

public class SkillManager : MonoBehaviour
{
    public static SkillManager Instance;
    private Dictionary<string, bool> skills;
    
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            print("Passe");
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            skills = new Dictionary<string, bool>();
            skills["criticalStrike"] = false;
            skills["fireball"] = false;
            skills["fireballCriticalStrike"] = false;
            skills["enemyGoldOnHit"] = false;
            skills["fortune"] = false;
        }
    }

    public Dictionary<string, bool> GetSkills()
    {
        return skills;
    }

    public void SetSkills(Dictionary<string, bool> s)
    {
        skills = s;
    }

    public void UnlockSkill(string skill)
    {
        skills[skill] = true;
    }
}