using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectiveManager : MonoBehaviour
{
    public int[] objectives;
    public int currentIndex;
    public int currentObjective;
    public int currentAdvancement;
    public GameObject bossTimer;
    private GameObject bossButton;
    public GameObject[] asteroids1;
    public GameObject[] asteroids2;
    public GameObject[] asteroids3;
    public GameObject[] enemies;
    private List<GameObject[]> allAsteroids;

    public static ObjectiveManager Instance;
    
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            print("Passe");
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            if (objectives.Length > 0)
            {
                currentObjective = objectives[0];
            }
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        allAsteroids = new List<GameObject[]>();
        allAsteroids.Add(asteroids1);
        allAsteroids.Add(asteroids2);
        allAsteroids.Add(asteroids3);
        bossButton = GameObject.FindWithTag("bossButton");
    }

    private void Update()
    {
        if (GameManager.Instance.levelState == State.Level)
        {
            float fillPercentage = (float)currentAdvancement / currentObjective;
            bossButton.GetComponent<Button>().interactable = (fillPercentage >= 1);
            bossButton.GetComponent<Image>().fillAmount = fillPercentage;
            bossTimer.SetActive(false);
        }

    }

    public void GoToBoss()
    {
        bossTimer.SetActive(true);
        bossButton.GetComponent<Button>().interactable = false;
        GameObject enemy = GameObject.FindWithTag("enemy");
        GameManager.Instance.SetBossComponents(enemy);
    }

    public void AddAdvancement(int value)
    {
        currentAdvancement += value;
    }

    public void NextObjective()
    {
        if (currentIndex < objectives.Length - 1)
        {
            currentIndex++;
            currentObjective = objectives[currentIndex];
            currentAdvancement = 0;
        }
    }

    public void ResetCurrentObjective()
    {
        currentAdvancement = 0;
    }

    public GameObject[] GetAsteroids()
    {
        return allAsteroids[currentIndex];
    }

    public GameObject GetEnemy()
    {
        return enemies[currentIndex];
    }
}
