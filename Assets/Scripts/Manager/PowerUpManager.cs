using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpManager : MonoBehaviour
{
    private float currentShieldTime, maxShieldTime;
    private float currentMultiShotTime, maxMultiShotTime;
    private float currentSpeedTime, maxSpeedTime;
    private float currentFireballTime, maxFireballTime;
    
    private GameObject myShip, shipShield;
    private GameObject shieldImage, multiShotImage, speedImage, fireballImage;
    private Image shieldFill, multiShotFill, speedFill, fireballFill;
    public static PowerUpManager Instance;
    
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    void Start()
    {
        myShip = GameObject.FindWithTag("myShip");
        shipShield = GameObject.FindWithTag("shipShield");
        shipShield.SetActive(false);
        
        shieldImage = GameObject.FindWithTag("shieldDisplay");
        speedImage = GameObject.FindWithTag("speedDisplay");
        multiShotImage = GameObject.FindWithTag("multiShotDisplay");
        fireballImage = GameObject.FindWithTag("fireballDisplay");
        shieldFill = GameObject.FindWithTag("shieldFill").GetComponent<Image>();
        speedFill = GameObject.FindWithTag("speedFill").GetComponent<Image>();
        multiShotFill = GameObject.FindWithTag("multiShotFill").GetComponent<Image>();
        fireballFill = GameObject.FindWithTag("fireballFill").GetComponent<Image>();
        
        shieldImage.SetActive(false);
        speedImage.SetActive(false);
        multiShotImage.SetActive(false);
        fireballImage.SetActive(false);
    }
    
    void Update()
    {
        if (GameManager.Instance.gameState == State.Play)
        {
            if (maxShieldTime != 0)
            {
                currentShieldTime += Time.deltaTime;
                float fillPercentage = currentShieldTime / maxShieldTime;
                shieldFill.fillAmount = fillPercentage;
                if (currentShieldTime >= maxShieldTime)
                {
                    RemoveShieldBoost();
                }
            }
            if (maxMultiShotTime != 0)
            {
                currentMultiShotTime += Time.deltaTime;
                float fillPercentage = currentMultiShotTime / maxMultiShotTime;
                multiShotFill.fillAmount = fillPercentage;
                if (currentMultiShotTime >= maxMultiShotTime)
                {
                    RemoveMultiShotBoost();
                }
            }
            if (maxSpeedTime != 0)
            {
                currentSpeedTime += Time.deltaTime;
                float fillPercentage = currentSpeedTime / maxSpeedTime;
                speedFill.fillAmount = fillPercentage;
                if (currentSpeedTime >= maxSpeedTime)
                {
                    RemoveSpeedBoost();
                }
            }
            if (maxFireballTime != 0)
            {
                currentFireballTime += Time.deltaTime;
                float fillPercentage = currentFireballTime / maxFireballTime;
                fireballFill.fillAmount = fillPercentage;
                if (currentFireballTime >= maxFireballTime)
                {
                    RemoveFireballBoost();
                }
            }
        }
    }

    public bool HasShield()
    {
        return maxShieldTime != 0;
    }

    public bool HasMultiShot()
    {
        return maxMultiShotTime != 0;
    }
    
    public void SetShieldBoost()
    {
        maxShieldTime += 6f;
        shipShield.SetActive(true);
        shieldImage.SetActive(true);
    }

    public void SetMultiShot()
    {
        if (myShip.GetComponent<Shoot>() != null)
        {
            Destroy(myShip.GetComponent<Shoot>());
        }
        if (myShip.GetComponent<MultiShoot>() == null)
        {
            myShip.AddComponent<MultiShoot>();
        }
        maxMultiShotTime += 6f;
        multiShotImage.SetActive(true);
    }

    public void SetSpeedBoost()
    {
        maxSpeedTime += 6f;
        float boost = myShip.GetComponent<ShipStats>().GetSpeed() * 0.4f;
        myShip.GetComponent<ShipStats>().SetPowerUpSpeed(boost);
        speedImage.SetActive(true);
    }
    
    public void SetFireballBoost()
    {
        maxFireballTime += 6f;
        if (gameObject.GetComponent<FireballSpawner>() == null)
        {
            gameObject.AddComponent<FireballSpawner>();
        }
        fireballImage.SetActive(true);
    }

    public void ClearPowerUps()
    {
        foreach (Transform pu in GameObject.FindWithTag("powerUpContainer").transform)
        {
            Destroy(pu.gameObject);
        }
    }

    public void RemoveMultiShotBoost()
    {
        maxMultiShotTime = 0;
        currentMultiShotTime = 0;
        multiShotImage.SetActive(false);
        if (myShip.GetComponent<MultiShoot>()!=null){
            Destroy(myShip.GetComponent<MultiShoot>());
            myShip.AddComponent<Shoot>();
        }
    }

    public void RemoveSpeedBoost()
    {
        maxSpeedTime = 0;
        currentSpeedTime = 0;
        myShip.GetComponent<ShipStats>().SetPowerUpSpeed(0);
        speedImage.SetActive(false);
    }

    public void RemoveShieldBoost()
    {
        maxShieldTime = 0;
        currentShieldTime = 0;
        shipShield.SetActive(false);
        shieldImage.SetActive(false);
    }

    public void RemoveFireballBoost()
    {
        Destroy(GetComponent<FireballSpawner>());
        maxFireballTime = 0;
        currentFireballTime = 0;
        fireballImage.SetActive(false);
    }

    public void ResetPowerUpsTimer()
    {
        RemoveMultiShotBoost();
        RemoveShieldBoost();
        RemoveSpeedBoost();
        RemoveShieldBoost();
        RemoveFireballBoost();
    }
    
}
