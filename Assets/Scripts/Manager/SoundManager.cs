using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;
    public AudioSource musicAudioSource, soundAudioSource;
    public Slider musicSlider, soundSlider;
    public AudioClip playerShotSound, bossWarningSound, bossMusic, mainMenuMusic, gameMusic;
    public Animator anim;
    private float soundVolume;
    public bool volumeOpen;
    
    // Start is called before the first frame update

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
            soundVolume = 0.5f;
            musicAudioSource.volume = 0.5f;
            PlayMainMenuMusic();
            DontDestroyOnLoad(gameObject);
        }
    }

    public void ShotSound()
    {
        MakeSound(playerShotSound);
    }

    public void WarningSound()
    {
        MakeSound(bossWarningSound);
    }

    public void MakeSound(AudioClip originalClip)
    {
        soundAudioSource.PlayOneShot(originalClip, soundVolume);
    }

    public void PlayMainMenuMusic()
    {
        PlayMusic(mainMenuMusic);
    }

    public void PlayGameMusic()
    {
        PlayMusic(gameMusic);
    }

    public void PlayBossMusic()
    {
        PlayMusic(bossMusic);
    }

    public void PlayMusic(AudioClip ac)
    {
        musicAudioSource.loop = true;
        musicAudioSource.clip = ac;
        musicAudioSource.Play(); 
    }

    public void StopMusic()
    {
        musicAudioSource.loop = false;
        musicAudioSource.Stop();
    }

    public void SetSoundVolume()
    {
        soundVolume = soundSlider.value;
    }

    public void SetMusicVolume()
    {
        musicAudioSource.volume = musicSlider.value;
    }
    
    public void TriggerVolume()
    {
        volumeOpen = !volumeOpen;
        if (volumeOpen)
        {
            if (MainMenu.Instance.tutorial.tutorialOpen)
            {
                MainMenu.Instance.tutorial.TriggerTutorial();
            }

            if (MainMenu.Instance.newGame.open)
            {
                MainMenu.Instance.newGame.Trigger();
            }
            Open();
        }
        else
        {
            Close();
        }
    }
    
    public void Open()
    {
        anim.SetBool("Open", true);
    }

    public void Close()
    {
        anim.SetBool("Open", false);
    }
}
