using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public enum State
{
    Play,
    Pause,
    Level,
    Boss
}

public class GameManager : MonoBehaviour
{
    private GameObject myShip;
    private GameObject shipShield;
    
    public static GameManager Instance;
    public State gameState, levelState;

    private float currentTime;
    
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    void Start()
    {
        myShip = GameObject.FindWithTag("myShip");
        gameState = State.Play;
        levelState = State.Level;
        SpawnEnemy();
        if (ControlManager.Instance.controlState == ControlState.TouchScreen)
        {
            GameObject.FindWithTag("joystick").SetActive(true);
        }
        else
        {
            GameObject.FindWithTag("joystick").SetActive(false);
        }
    }
    
    // Update is called once per frame
    void Update()
    {
        if (gameState == State.Play)
        {
            currentTime += Time.deltaTime;
            if (currentTime > 1)
            {
                SavePlayer();
                currentTime = 0;
            }
        }
        if (myShip != null)
        {
            GameObject.FindWithTag("goldText").GetComponent<Text>().text = "" + NumberFact.Fact(myShip.GetComponent<ShipStats>().GetGold());
            GameObject.FindWithTag("lifeText").GetComponent<Text>().text = "x" + myShip.GetComponent<ShipStats>().GetLife();
        }  
    }

    public void ClearAsteroids()
    {
        foreach (GameObject go in GameObject.FindGameObjectsWithTag("asteroid"))
        {
            go.GetComponent<AsteroidStats>().DestroyHealthBar();
            Destroy(go);
        }
    }

    public void SetAsteroidSpawnWork(bool work)
    {
        GameObject.FindWithTag("asteroidsContainer").GetComponent<AsteroidSpawner>().SetWork(work);
    }

    public void AddPlayerExp(int exp)
    {
        GameObject.FindWithTag("expBar").GetComponent<ShipExperienceBar>().AddExp(exp);
    }

    public void AddPlayerGold(int gold)
    {
        myShip.GetComponent<ShipStats>().AddGold(gold);
    }

    public void RemoveLife()
    {
        if (myShip.GetComponent<FadeOut>() == null && myShip.GetComponent<InvincibleShip>() == null && !PowerUpManager.Instance.HasShield())
        {
            myShip.GetComponent<ShipStats>().RemoveLife();
            if (myShip.GetComponent<ShipStats>().IsAlive())
            {
                ShipInvulnerable();
            }
            else
            {
                myShip.AddComponent<FadeOut>();
            }
        }
    }

    public void AddPlayerLife()
    {
        myShip.GetComponent<ShipStats>().AddLife();
    }

    public void RemoveHp(AsteroidStats asteroid, bool crit)
    {
        float shipDamage = myShip.GetComponent<ShipStats>().GetAttack();
        if (crit)
        {
            Debug.Log("CriticalStrike !");
            shipDamage = myShip.GetComponent<ShipStats>().GetCriticalAttack();
        }
        if (PowerUpManager.Instance.HasMultiShot())
        {
            shipDamage *= 0.75f;
        }
        asteroid.RemoveHp(shipDamage);
    }

    public void RemoveHp(EnemyStats enemy, bool crit)
    {
        if (enemy.gameObject.GetComponent<FadeOut>() == null)
        {
            float shipDamage = myShip.GetComponent<ShipStats>().GetAttack();
            if (crit)
            {
                Debug.Log("CriticalStrike !");
                shipDamage = myShip.GetComponent<ShipStats>().GetCriticalAttack();
            }
            if (PowerUpManager.Instance.HasMultiShot())
            {
                shipDamage *= 0.75f;
            }
            enemy.RemoveHp(shipDamage);
            if (!enemy.IsAlive())
            {
                AddPlayerGold(enemy.goldOnKill);
                AddPlayerExp(enemy.expOnKill);
                enemy.AddComponent<BossDestruction>();
                enemy.AddComponent<FadeOut>();
            }
        }
    }

    public void ShipInvulnerable()
    {
        myShip.AddComponent<InvincibleShip>();
    }

    public void SetBossComponents(GameObject boss)
    {
        levelState = State.Boss;
        SetAsteroidSpawnWork(false);
        ClearAsteroids();
        
        Destroy(boss.GetComponent<EnemyMovement>());
        Destroy(boss.GetComponent<EnemyShoot>());
        Destroy(boss.GetComponent<EnemyCollider>());
        boss.AddComponent<BossMovement>();
        boss.AddComponent<BossShoot>();
        boss.AddComponent<BossCollider>();
        boss.AddComponent<BossPowerUpSpawner>();
        SoundManager.Instance.PlayBossMusic();
    }

    public void RespawnShip()
    {
        ClearAsteroids();
        ShipStats stats = myShip.GetComponent<ShipStats>();
        stats.SetLife(3);
        stats.SetGold(stats.GetGold()/2);
        
        myShip.transform.position = Vector3.zero;
        Color color = myShip.GetComponent<SpriteRenderer>().color;
        color.a = 1;
        myShip.GetComponent<SpriteRenderer>().color = color;
        
        if (levelState == State.Boss)
        {
            SetAsteroidSpawnWork(true);
            SoundManager.Instance.PlayGameMusic();
        }
        
        levelState = State.Level;
        DestroyEnemy();
        SpawnEnemy();
        PowerUpManager.Instance.ClearPowerUps();
        PowerUpManager.Instance.ResetPowerUpsTimer();
    }
    
    public void RespawnShipTimeUp()
    {
        ClearAsteroids();
        myShip.transform.position = Vector3.zero;
        Color color = myShip.GetComponent<SpriteRenderer>().color;
        color.a = 1;
        myShip.GetComponent<SpriteRenderer>().color = color;
        SetAsteroidSpawnWork(true);
        SoundManager.Instance.PlayGameMusic();
        levelState = State.Level;
        DestroyEnemy();
        SpawnEnemy();
        PowerUpManager.Instance.ClearPowerUps();
        PowerUpManager.Instance.ResetPowerUpsTimer();
    }

    public void DestroyEnemy()
    {
        GameObject boss = GameObject.FindWithTag("enemy");
        boss.GetComponent<EnemyStats>().DestroyHealthBar();
        Destroy(boss);
    }
    
    public void SpawnEnemy()
    {
        foreach (Transform tf in GameObject.FindWithTag("enemyShots").transform)
        {
            Destroy(tf.gameObject);
        }
        GameObject go = Instantiate(ObjectiveManager.Instance.GetEnemy(), new Vector3(20, 0, 0), Quaternion.Euler(0f, -180f, 0f));
    }

    // --------- SAVE / LOAD FUNCTIONS ----------------
    public void SavePlayer()
    {
        SaveSystem.SavePlayer(myShip.GetComponent<ShipStats>());
    }

    public void LoadPlayer()
    {
        
        PlayerData data = SaveSystem.LoadPlayer();
        BasicUpgradesManager upgrades = GameObject.FindWithTag("managers").GetComponent<BasicUpgradesManager>();
        ShipStats s = myShip.GetComponent<ShipStats>();
    
        s.SetLife(data.life);

        s.SetBonusBaseAttack(data.bonusBaseAttack);
        s.SetBonusBaseAttackSpeed(data.bonusBaseAttackSpeed);
        s.SetBonusBaseSpeed(data.bonusBaseSpeed);

        s.SetExpLevel(data.level);
        s.SetCurrentExp(data.currentExp);
        s.SetMaxExp(data.maxExp);
        s.SetGold(data.gold);

        Vector3 newPos = new Vector3(data.position[0], data.position[1], data.position[2]);
        s.gameObject.transform.position = newPos;
        
        ObjectiveManager.Instance.currentIndex = data.currentIndex;
        ObjectiveManager.Instance.currentAdvancement = data.currentAdvancement;
        ObjectiveManager.Instance.currentObjective = data.currentObjective;
        
        DestroyEnemy();
        SpawnEnemy();
        
        upgrades.CalculateAllCost(data.shopAttackLevel, data.shopAttackSpeedLevel, data.shopSpeedLevel);
        s.RefreshStats();

        if (data.skills != null)
        {
            SkillManager.Instance.SetSkills(data.skills);
        }
    }
}
