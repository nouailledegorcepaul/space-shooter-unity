using UnityEngine;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour
{
    public Animator shopWindowAnim;
    public GameObject basicUpgrades, skills;
    public Button basicUpgradesButton, skillsButton;
    private bool open;

    private void Start()
    {
        open = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (open)
            {
                CloseShop();
            }
            else
            {
                OpenShop(); 
            }
        }
    }

    public void OpenShop()
    {
        open = true;
        DisplayBasicUpgrades();
        shopWindowAnim.Play("shopMenuOpen");
        GameManager.Instance.gameState = State.Pause;
    }
    
    public void CloseShop()
    {
        open = false;
        shopWindowAnim.Play("shopMenuClose");
        GameManager.Instance.gameState = State.Play;
    }

    public void DisplayBasicUpgrades()
    {
        basicUpgrades.SetActive(true);
        skills.SetActive(false);
        basicUpgradesButton.interactable = false;
        skillsButton.interactable = true;
        
    }

    public void DisplaySkills()
    {
        skills.SetActive(true);
        basicUpgrades.SetActive(false);
        skillsButton.interactable = false;
        basicUpgradesButton.interactable = true;
        
    }
}
