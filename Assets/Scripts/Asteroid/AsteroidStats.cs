using UnityEngine;

public class AsteroidStats : MonoBehaviour
{
    public int difficulty;
    public float hp;
    public int goldOnKill;
    public int expOnKill;
    private Vector3 healthBarLocalScale;
    private float maxLife;
    private GameObject healthBar;
    private float dist;
    
    private void Start(){
        maxLife = hp;
        if (SkillManager.Instance.GetSkills()["fortune"])
        {
            if (Random.Range(0, 100) < 2)
            {
                SetGoldenAsteroid();
            } 
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (healthBar != null)
        {
            if (hp < 0)
            {
                hp = 0;
            }

            float fill = hp / maxLife;
            if (fill < 1)
            {
                healthBar.SetActive(true);
                healthBarLocalScale.x = fill;
                healthBar.GetComponent<SpriteRenderer>().transform.localScale = healthBarLocalScale;
            }
            else
            {
                healthBar.SetActive(false);
            }
            Vector3 goPos = gameObject.transform.position;
            healthBar.transform.position = new Vector3(goPos.x, goPos.y + dist, goPos.z);
            if (!IsAlive())
            {
                if (GetComponent<FadeOut>() == null)
                {
                    gameObject.AddComponent<FadeOut>();
                    PowerUpSpawner.Instance.SpawnPowerUp(transform.position);
                    GameManager.Instance.AddPlayerExp(expOnKill);
                    GameManager.Instance.AddPlayerGold(goldOnKill);
                    ObjectiveManager.Instance.AddAdvancement(difficulty);
                    DestroyHealthBar();
                }
            }   
        }
    }

    public void RemoveHp(float amount)
    {
        hp -= amount;
    }
    public bool IsAlive()
    {
        return hp > 0;
    }

    public GameObject GetHealthBar()
    {
        return healthBar;
    }

    public void SetGoldenAsteroid()
    {
        expOnKill *= 5;
        goldOnKill *= 5;
        healthBar.GetComponent<SpriteRenderer>().color = new Color32(140, 131, 41, 255);
        GetComponent<SpriteRenderer>().color = new Color32(255, 255, 0, 255);
    }

    public void SetHealthBar(GameObject go)
    {
        Vector3 topRightCamBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        healthBar = go;
        healthBarLocalScale = healthBar.GetComponent<SpriteRenderer>().transform.localScale;
        Vector2 size = healthBar.GetComponent<SpriteRenderer>().bounds.size;
        if (healthBar.GetComponent<SpriteRenderer>().transform.position.y + size.y/2 > topRightCamBorder.y)
        {
            Vector3 currPos = healthBar.GetComponent<SpriteRenderer>().transform.localScale;
            currPos.y = -currPos.y;
            healthBar.GetComponent<SpriteRenderer>().transform.localScale = currPos;
        }
        Vector2 size2 = GetComponent<SpriteRenderer>().bounds.size;
        float d = size2.y / 2 + 0.1f;
        dist = d;
    }

    public void DestroyHealthBar()
    {
        Destroy(healthBar);
    }
}
