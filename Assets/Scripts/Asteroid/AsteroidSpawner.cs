using UnityEngine;
using Random = UnityEngine.Random;

public class AsteroidSpawner : MonoBehaviour
{
    private Vector3 rightTopCamBorder;
    private Vector3 rightBotCamBorder;
    
    private bool work;

    // Start is called before the first frame update
    void Start()
    {
        rightTopCamBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        rightBotCamBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        work = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.gameState == State.Play)
        {
            if (work)
            {
                GameObject[] currentAsteroids = GameObject.FindGameObjectsWithTag("asteroid");
                int rand = Random.Range(1, 100);
                if (currentAsteroids.Length < 10)
                {
                    if (rand == 50 || currentAsteroids.Length < 4)
                    {
                        int index = 0;
                        rand = Random.Range(1, 100);
                        if (rand == 1)
                        {
                            index = 2;
                        }
                        else if (rand < 15)
                        {
                            index = 1;
                        }
                        SpawnAsteroid(ObjectiveManager.Instance.GetAsteroids()[index]);
                    }
                }  
            }
        }
    }

    public void SpawnAsteroid(GameObject asteroid)
    {
        GameObject healthBar = Resources.Load("healthBar") as GameObject;
        if (asteroid != null && healthBar != null)
        {
            Vector2 size = asteroid.GetComponent<SpriteRenderer>().bounds.size;
            float y = Random.Range(rightBotCamBorder.y + size.y / 2, rightTopCamBorder.y - size.y / 2);
            Vector3 newPos = new Vector3(rightTopCamBorder.x + 2*size.x, y, transform.position.z);
            
            GameObject go = Instantiate(asteroid, newPos, Quaternion.identity);
            GameObject hb = Instantiate(healthBar, newPos, Quaternion.identity);
            go.GetComponent<AsteroidStats>().SetHealthBar(hb);
            go.transform.SetParent(GameObject.FindGameObjectWithTag("asteroidsContainer").transform);
            hb.transform.SetParent(GameObject.FindGameObjectWithTag("asteroidsContainer").transform);

            
        }
        else
        {
            Debug.Log("GameObject not found");
        }
    }

    public void SetWork(bool value)
    {
        work = value;
    }
}
