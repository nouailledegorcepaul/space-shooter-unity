using UnityEngine;
using Random = UnityEngine.Random;

public class AsteroidMovement : MonoBehaviour
{
    private SpriteRenderer rend;
    private Vector3 leftBotCamBorder;
    public float speed;
    public float rotationSpeed;
    private int direction;
    private bool collResist;
    private float time;

    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<SpriteRenderer>();
        leftBotCamBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        collResist = false;
        direction = 1;
        if (Random.Range(0, 2) == 1)
        {
            direction = -1;
        }
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if (time > 1)
        {
            collResist = true;
        }
        Vector2 movement = new Vector2(0, 0);
        float rotation = 0;
        if (GameManager.Instance.gameState == State.Play)
        {
            movement = new Vector2(-speed, 0f);
            rotation = rotationSpeed;
        }
        GetComponent<Rigidbody2D>().velocity = movement;
        transform.Rotate(0, 0, rotation * direction * Time.deltaTime);
        Bounds bounds = rend.bounds;
        Vector3 size = bounds.size;
        float sizeX = size.x;
        Vector3 pos = transform.position;
        if (pos.x + (sizeX / 2) < leftBotCamBorder.x)
        {
            gameObject.GetComponent<AsteroidStats>().DestroyHealthBar();
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("asteroid") && !collResist)
        {
            if (gameObject.GetComponent<AsteroidStats>().GetHealthBar() != null)
            {
                gameObject.GetComponent<AsteroidStats>().DestroyHealthBar();
            }
            Destroy(gameObject);
        }
        else if (other.CompareTag("shipShot") && !collResist)
        {
            GameObject go = Instantiate(Resources.Load("hitExplosion"), other.transform.position, Quaternion.identity) as GameObject;
            GameManager.Instance.RemoveHp(GetComponent<AsteroidStats>(), other.GetComponent<ShootMovement>().IsCritical());
            Destroy(gameObject);
        }
    }
}
