using UnityEngine;

public class ShipTutorialMovement : MonoBehaviour
{
    public Vector3 startPos;
    private readonly string[] directions = {"up", "right", "bot", "left"};
    private Vector2 movement;
    private float currentTime;
    private int currentIndex;
    public float speed;

    public bool move;
    
    // Start is called before the first frame update
    void Start()
    {
        currentIndex = 0;
        ApplyMovement();
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        if (move)
        {
            if (currentTime < 1.5)
            {
                GetComponent<Rigidbody2D>().velocity = movement * speed;
            }
            else{
                Vector2 reset = new Vector2(-movement.x, -movement.y) * speed;
                GetComponent<Rigidbody2D>().velocity = reset;
                if (Vector2.Distance(GetComponent<RectTransform>().anchoredPosition, startPos) < 0.03)
                {
                    GetComponent<RectTransform>().anchoredPosition = startPos;
                    currentIndex++;
                    if (currentIndex == 4)
                    {
                        currentIndex = 0;
                    }
                    ApplyMovement();
                    currentTime = 0;
                }
            }  
        }
        else
        {
            if (currentTime > 2f)
            {
                Vector3 ap = GetComponent<RectTransform>().anchoredPosition;
                Vector2 size = GetComponent<RectTransform>().sizeDelta;
                GameObject go = Instantiate(Resources.Load("tutorialShot"), Vector3.zero, Quaternion.Euler(new Vector3(0, 0, 90))) as GameObject;
                go.transform.SetParent(GameObject.FindWithTag("tutorialShotContainer").transform, true);
                go.GetComponent<RectTransform>().anchoredPosition = new Vector3(ap.x + size.x/2, ap.y, ap.z);
                currentTime = 0;
            }
        }
    }

    public void ApplyMovement()
    {
        string direction = directions[currentIndex];
        switch (direction)
        {
            case "up":
                movement = new Vector2(0, 1);
                break;
            case "right":
                movement = new Vector2(1, 0);
                break;
            case "left":
                movement = new Vector2(-1, 0);
                break;
            case "bot" :
                movement = new Vector2(0, -1);
                break;
        }
    }
}
