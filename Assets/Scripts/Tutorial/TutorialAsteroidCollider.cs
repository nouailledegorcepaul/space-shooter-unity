﻿using UnityEngine;

public class TutorialAsteroidCollider : MonoBehaviour
{
    public TutorialObjectiveManager manager;
    private void OnTriggerEnter2D(Collider2D other)
    {
        Destroy(other.gameObject);
        manager.DestroyAsteroid();
    }
}
