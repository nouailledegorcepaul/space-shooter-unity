using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class TutorialObjectiveManager : MonoBehaviour
{
    public GameObject asteroid;
    public GameObject bossButton;
    public GameObject finger;

    private float currentTime, fingerTime;
    private int currentAdvancement, maxAdvancement;

    // Start is called before the first frame update
    void Start()
    {
        currentAdvancement = 0;
        maxAdvancement = 5;
        RespawnAsteroid();
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        if (currentTime > 1f && !asteroid.activeSelf)
        {
            RespawnAsteroid();
        }
        
        float fillPercentage = (float)currentAdvancement / maxAdvancement;
        bossButton.GetComponentInChildren<Button>().interactable = (fillPercentage >= 1);
        bossButton.GetComponentsInChildren<Image>()[0].fillAmount = fillPercentage;
        if (fillPercentage >= 1f)
        {
            fingerTime += Time.deltaTime;
            if (fingerTime > 0.3)
            {
                if (finger.activeSelf)
                {
                    finger.SetActive(false);
                }
                else
                {
                    finger.SetActive(true);
                }
                fingerTime = 0;
            }
        }
        else
        {
            fingerTime = 0;
            finger.SetActive(false);
        }
    }
    

    public void DestroyAsteroid()
    {
        asteroid.SetActive(false);
        currentTime = 0;
        currentAdvancement++;
        if (currentAdvancement > maxAdvancement + 1)
        {
            currentAdvancement = 0;
        }
    }

    public void RespawnAsteroid()
    {
        if (!finger.activeSelf)
        {
            asteroid.SetActive(true);
        }
    }
}
