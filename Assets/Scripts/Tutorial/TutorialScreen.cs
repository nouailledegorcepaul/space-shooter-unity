using Unity.VisualScripting;
using UnityEngine;

public class TutorialScreen : MonoBehaviour
{
    public GameObject[] pages;
    public GameObject previousButton, nextButton;
    private int currentIndex;

    public bool tutorialOpen;
    public Animator anim;
    
    // Start is called before the first frame update
    void Start()
    {
        tutorialOpen = false;
    }

    public void Next()
    {
        pages[currentIndex].SetActive(false);
        currentIndex++;
        CheckIndex();
    }

    public void Previous()
    {
        pages[currentIndex].SetActive(false);
        currentIndex--;
        CheckIndex();
    }

    public void CheckIndex()
    {
        pages[currentIndex].SetActive(true);
        if (currentIndex > 0)
        {
            previousButton.SetActive(true);
        }
        else
        {
            previousButton.SetActive(false);
        }
        if (currentIndex < pages.Length-1)
        {
            nextButton.SetActive(true);
        }
        else
        {
            nextButton.SetActive(false);
        }
    }

    public void TriggerTutorial()
    {
        tutorialOpen = !tutorialOpen;
        if (tutorialOpen)
        {
            if (MainMenu.Instance.leaderboard.leaderBoardOpen)
            {
                MainMenu.Instance.leaderboard.Trigger();
            }

            if (SoundManager.Instance.volumeOpen)
            {
                SoundManager.Instance.TriggerVolume();
            }

            if (MainMenu.Instance.newGame.open)
            {
                MainMenu.Instance.newGame.Trigger();
            }
            Open();
        }
        else
        {
            Close();
        }
    }

    public void Open()
    {
        if (!UsernameCreator.Instance.creatorOpen)
        {
            anim.SetBool("Open", true);
            currentIndex = 0;
            foreach (GameObject go in pages)
            {
                go.SetActive(false);
            }
            CheckIndex();
        }
    }

    public void Close()
    {
        anim.SetBool("Open", false);
    }
}
