using UnityEngine;

public class TutorialShot : MonoBehaviour
{
    private float currentTime;

    public bool objective;
    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        GetComponent<Rigidbody2D>().velocity = new Vector2(0.5f, 0);

        if (!objective)
        {
            if (currentTime > 1.5f)
            {
                Destroy(gameObject);
            }  
        }
    }
}
