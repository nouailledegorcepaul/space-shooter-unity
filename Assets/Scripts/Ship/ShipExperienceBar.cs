using System;
using UnityEngine;
using UnityEngine.UI;

public class ShipExperienceBar : MonoBehaviour
{
    // public Slider slider;
    private ShipStats stats;
    public Image fill;

    private void Start()
    {
        stats = GameObject.FindWithTag("myShip").GetComponent<ShipStats>();
    }

    void Update()
    {
        float fillPercentage = (float)stats.GetCurrentExp() / stats.GetMaxExp();
        fill.fillAmount = fillPercentage;
        GameObject.FindWithTag("levelText").GetComponent<Text>().text = "Level: " + stats.GetExpLevel();
        GameObject.FindWithTag("expText").GetComponent<Text>().text = "Exp: " + stats.GetCurrentExp() + "/" + stats.GetMaxExp();
    }

    public void AddExp(int exp)
    {
        stats.AddExp(exp);
        if (stats.GetCurrentExp() >= stats.GetMaxExp())
        {
            LevelUp();
        }
    }

    private void LevelUp()
    {
        int expExcess = stats.GetCurrentExp() - stats.GetMaxExp();
        float mult = 1.3f;
        if (stats.GetExpLevel() > 10)
        {
            mult = 2f;
        }
        stats.LevelUp();
        int newMaxExp = (int) (stats.GetMaxExp() * mult);
        stats.SetMaxExp(newMaxExp);
        stats.SetCurrentExp(0);
        // slider.maxValue = stats.GetMaxExp();
        GameObject.FindWithTag("levelText").GetComponent<Text>().text = "Level: " + stats.GetExpLevel();
        AddLevelUpStats();
        PlayFabManager.Instance.SendLeaderboard(stats.GetExpLevel());
        
        AddExp(expExcess);
    }
    
    public void AddLevelUpStats()
    {
        if (GameObject.FindWithTag("myShip") != null)
        {
            ShipStats stats = GameObject.FindWithTag("myShip").GetComponent<ShipStats>();
            int level = stats.GetExpLevel();
        
            float bonusAttack = GrowthFormula(stats.GetBaseAttack(), stats.GetAttackGrowth(), level);
            stats.SetBonusBaseAttack(bonusAttack);
        
            float bonusAttackSpeed = GrowthFormula(stats.GetBaseAttackSpeed(), stats.GetAttackSpeedGrowth(), level);
            stats.SetBonusBaseAttackSpeed(bonusAttackSpeed);
        
            float bonusSpeed = GrowthFormula(stats.GetBaseSpeed(), stats.GetSpeedGrowth(), level);
            stats.SetBonusBaseSpeed(bonusSpeed);
        
            stats.RefreshStats();
        }
    }

    public float GrowthFormula(float baseStat, float growth, int level)
    {
        return baseStat + growth * (level - 1f) * (0.7025f + 0.0175f * (level - 1f));
    }
}
