using UnityEngine;

public class ShipStats : MonoBehaviour
{

    public int life;
    private float baseAttackSpeed, baseAttack, baseSpeed;
    private float bonusBaseAttackSpeed, bonusBaseAttack, bonusBaseSpeed;
    private float bonusAttackSpeed, bonusAttack, bonusSpeed;
    private float attackSpeedGrowth, attackGrowth, speedGrowth;
    public float attackSpeed, attack, speed;
    private float powerUpSpeed;

    private int currentExp, maxExp;
    private int level;
    public int gold;
    
    private bool canAttack;
    private float attackTimer;

    void Start()
    {
        baseAttack = 0.5f;
        baseAttackSpeed = 0.8f;
        baseSpeed = 2f;
        currentExp = 0;
        maxExp = 10;
        level = 1;
        gold = 0;
        canAttack = true;
        
        attackGrowth = 0.05f;
        attackSpeedGrowth = 0.01f;
        speedGrowth = 0.001f;
        
        bonusBaseAttack = 0.5f;
        bonusBaseAttackSpeed = 0.8f;
        bonusBaseSpeed = 2f;
        
        
        bonusAttack = 0;
        bonusSpeed = 0;
        bonusAttackSpeed = 0;
        
        powerUpSpeed = 0;
        
        GameObject saveLoader = GameObject.FindWithTag("saveLoader");
        if (saveLoader != null)
        {
            GameManager.Instance.LoadPlayer();
        }
    }

    private void Update()
    {
        RefreshStats();
        if (!canAttack)
        {
            attackTimer += Time.deltaTime;
            if (attackTimer > 1.0f / attackSpeed)
            {
                attackTimer = 0;
                canAttack = true;
            }
        }
    }

    // GETTER
    public float GetBaseAttack() { return baseAttack; }
    public float GetAttack() { return attack; }
    public float GetCriticalAttack() { return attack * GetComponent<ShipCrit>().GetCriticalDamage(); }
    public float GetAttackGrowth() { return attackGrowth; }

    public float GetBaseAttackSpeed() { return baseAttackSpeed; }
    public float GetAttackSpeed() { return attackSpeed; }
    public float GetAttackSpeedGrowth() { return attackSpeedGrowth; }
    
    public float GetBaseSpeed() { return baseSpeed; }
    public float GetSpeed() { return speed; }
    public float GetSpeedGrowth() { return speedGrowth; }
    public float GetPowerUpSpeed() { return powerUpSpeed; }

    public float GetBonusBaseAttack() { return bonusBaseAttack;}
    public float GetBonusBaseAttackSpeed() { return bonusBaseAttackSpeed;}
    public float GetBonusBaseSpeed() { return bonusBaseSpeed;}
    
    public float GetBonusAttack() { return bonusAttack;}
    public float GetBonusAttackSpeed() { return bonusAttackSpeed;}
    public float GetBonusSpeed() { return bonusSpeed;}
    
    public int GetExpLevel() { return level; }
    public int GetCurrentExp() { return currentExp; }
    public int GetMaxExp() { return maxExp; }
    
    public int GetLife() { return life; }
    public bool IsAlive() { return life > 0; }

    public int GetGold() { return gold; }
    public bool CanAttack() { return canAttack; }
    
    // SETTER
    public void SetLife(int value) { life = value;}
    public void RemoveLife() { life--; }
    public void AddLife() { life++; }
    public void AddGold(int amount) { gold += amount; }
    public void AddExp(int exp) { currentExp += exp; }
    
    public void SetExpLevel(int l) { level = l;}
    public void SetMaxExp(int exp) { maxExp = exp; }
    public void SetCurrentExp(int exp) { currentExp = exp;}
    
    public void SetBaseAttack(float value) { baseAttack = value; }
    public void SetBaseSpeed(float value) { baseSpeed = value; }
    public void SetBaseAttackSpeed(float value) { baseAttackSpeed = value; }
    
    public void SetBonusAttack(float value) { bonusAttack = value;}
    public void SetBonusAttackSpeed(float value) { bonusAttackSpeed = value;}
    public void SetBonusSpeed(float value) { bonusSpeed = value;}

    public void AddBonusAttack(float atk) { bonusAttack += atk; }
    public void AddBonusAttackSpeed(float atkSpd) { bonusAttackSpeed += atkSpd; }
    public void AddBonusSpeed(float spd) { bonusSpeed += spd; }

    public void SetBonusBaseAttack(float value) { bonusBaseAttack = value;}
    public void SetBonusBaseAttackSpeed(float value) { bonusBaseAttackSpeed = value;}
    public void SetBonusBaseSpeed(float value) { bonusBaseSpeed = value;}

    public void SetPowerUpSpeed(float powerUp) { powerUpSpeed = powerUp; }

    public void SetGold(int value) { gold = value; }

    public void SetCanAttack(bool value) { canAttack = value; }
    public void LevelUp() {level++; }

    public void SpendGold(int amount) { gold -= amount;}

    public void RefreshStats()
    {
        attack = bonusBaseAttack + bonusAttack;
        attackSpeed = bonusBaseAttackSpeed + bonusAttackSpeed;
        speed = bonusBaseSpeed + bonusSpeed + powerUpSpeed;
        
        Vector2 newSpeed = new Vector2(speed, speed);
        GetComponent<ShipMovement>().SetSpeed(newSpeed);
    }
}
