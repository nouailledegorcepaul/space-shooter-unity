using UnityEngine;

public class ShipShield : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("enemyShot"))
        {
            GameObject go = Instantiate(Resources.Load("hitExplosion"), other.transform.position, Quaternion.identity) as GameObject;
            Destroy(other.gameObject);
        }
    }
}
