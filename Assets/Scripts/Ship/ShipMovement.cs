using UnityEngine;

public class ShipMovement : MonoBehaviour
{
    private Vector2 speed;
    private Animator anim;
    public Joystick joystick;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        float inputY;
        float inputX;
        if (ControlManager.Instance.controlState == ControlState.TouchScreen)
        {
            inputY = joystick.Vertical;
            inputX = joystick.Horizontal;
        }
        else
        {
            inputY = Input.GetAxis("Vertical");
            inputX = Input.GetAxis("Horizontal");
        }
        Vector2 movement = new Vector2(0, 0);
        if (GameManager.Instance.gameState == State.Play)
        {
            movement = new Vector2(speed.x * inputX, speed.y * inputY);
            if (inputY != 0)
            {
                float angle = transform.localEulerAngles.z;
                angle = (angle > 180) ? angle - 360 : angle;
                if (inputY > 0){
                    if (angle < 20)
                    {
                        transform.Rotate(0, 0, 30f * Time.deltaTime);
                    }
                }
                else
                {
                    if (angle > -20)
                    {
                        transform.Rotate(0, 0, -30f * Time.deltaTime);
                    }
                }
            }
            else
            {
                transform.rotation = Quaternion.identity;
            }
            bool moving = inputY != 0 || inputX != 0;
            anim.SetBool("Moving", moving);
        }
        GetComponent<Rigidbody2D>().velocity = movement;
    }

    public void SetSpeed(Vector2 spd)
    {
        speed = spd;
    }
    
    void OnTriggerEnter2D(Collider2D other) {
        if(other.CompareTag("asteroid"))
        {
            if (other.gameObject.GetComponent<FadeOut>() == null)
            {
                GameManager.Instance.RemoveLife();
            }
        }
    }
    
    void OnTriggerStay2D(Collider2D other) {
        if(other.CompareTag("asteroid"))
        {
            if (other.gameObject.GetComponent<FadeOut>() == null)
            {
                GameManager.Instance.RemoveLife();
            }
        }
    }
}
