using UnityEngine;
using Random = UnityEngine.Random;

public class ShipCrit : MonoBehaviour
{
    private int critChance;
    private float critDamage;

    void Start()
    {
        critChance = 0;
        critDamage = 1;
    }

    public bool IsCriticalStrike()
    {
        int rand = Random.Range(0, 100);
        return rand < critChance;
    }

    public float GetCriticalDamage()
    {
        return critDamage;
    }

    public void AddCritChance(int c)
    {
        critChance += c;
    }

    public void AddCritDamage(float d)
    {
        critDamage += d;
    }
}
