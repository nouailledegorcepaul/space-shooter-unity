using UnityEngine;

public class ShipPos : MonoBehaviour
{
    private SpriteRenderer rend;

    private Vector3 rightTopCamBorder;
    private Vector3 rightBotCamBorder;
    private Vector3 leftTopCamBorder;
    private Vector3 leftBotCamBorder;
    

    // Start is called before the first frame update
    private void Start()
    {
        rend = GetComponent<SpriteRenderer>();
        rightTopCamBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        rightBotCamBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        leftTopCamBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        leftBotCamBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));

    }

    // Update is called once per frame
    private void Update()
    {
        Bounds bounds = rend.bounds;
        Vector3 size = bounds.size;
        Vector3 pos = transform.position;
        if (pos.x + (size.x/2) > rightBotCamBorder.x)
        {
            transform.position = new Vector3(rightBotCamBorder.x - (size.x/2), pos.y, pos.z);
        }
        if (pos.x - (size.x/2) < leftBotCamBorder.x)
        {
            transform.position = new Vector3(leftBotCamBorder.x + (size.x/2), pos.y, pos.z);
        }
        if (pos.y + (size.y/2) > leftTopCamBorder.y)
        {
            transform.position = new Vector3(pos.x, leftTopCamBorder.y - (size.y/2), pos.z);
        }
        if (pos.y - (size.y/2) < rightBotCamBorder.y)
        {
            transform.position = new Vector3(pos.x, rightBotCamBorder.y + (size.y/2), pos.z);
        }
    }
}
