using UnityEngine;

public class SaveLoader : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(transform);
    }
}
