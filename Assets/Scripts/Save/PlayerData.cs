using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public string username;
    
    public int life;
    
    public float bonusBaseAttackSpeed, bonusBaseAttack, bonusBaseSpeed;
    
    public int currentExp, maxExp;
    public int level;
    public int gold;


    public int currentObjective, currentAdvancement, currentIndex;
    public float[] position;

    public int shopAttackLevel, shopAttackSpeedLevel, shopSpeedLevel;

    public Dictionary<string, bool> skills;

    public PlayerData(ShipStats stats)
    {
        username = PlayFabManager.Instance.username;
        
        life = stats.GetLife();

        bonusBaseAttack = stats.GetBonusBaseAttack();
        bonusBaseAttackSpeed = stats.GetBonusBaseAttackSpeed();
        bonusBaseSpeed = stats.GetBonusBaseSpeed();

        level = stats.GetExpLevel();
        currentExp = stats.GetCurrentExp();
        maxExp = stats.GetMaxExp();
        gold = stats.GetGold();

        position = new float[3];
        position[0] = stats.gameObject.transform.position.x;
        position[1] = stats.gameObject.transform.position.y;
        position[2] = stats.gameObject.transform.position.z;

        currentIndex = ObjectiveManager.Instance.currentIndex;
        currentAdvancement = ObjectiveManager.Instance.currentAdvancement;
        currentObjective = ObjectiveManager.Instance.currentObjective;
        
        BasicUpgradesManager upgrades = GameObject.FindWithTag("managers").GetComponent<BasicUpgradesManager>();
        shopAttackLevel = upgrades.GetAttackLevel();
        shopAttackSpeedLevel = upgrades.GetAttackSpeedLevel();
        shopSpeedLevel = upgrades.GetSpeedLevel();
        
        skills = SkillManager.Instance.GetSkills();
    }
}
